﻿// Copyright (c) Stranger Games http://www.stranger-games.com/ . All rights reserved.
// Licensed under the GPLv2 license. See LICENSE file in the project root.

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.TreeViewExamples;
using UnityEngine;
using UnityEditor.IMGUI.Controls;
using StrangerGames.FastCompile.TreeViewExamples;

namespace StrangerGames.FastCompile {
	public class Common {

		static string dataPath;

		public const string InstructionsTitle = "New Library Created";
		public const string Instructions =
@"Press 'Open Solution' and start adding library code or open 'Project Folder' menu and add code through explorer";

		static string focusedControl = "";

		static Common() {
			dataPath = Application.dataPath;
		}

		public static string ConvertPath(string path) {
			if(string.IsNullOrEmpty(path))
				return path;
			var _baseDirectory = "";
			var directories = path.Split(new char[] { '\\', '/' });
			for(int i = 0; i < directories.Length; i++) {
				var dir = directories[i];
				if(!string.IsNullOrEmpty(dir))
					_baseDirectory += dir + Path.DirectorySeparatorChar;
			}
			_baseDirectory = _baseDirectory.Substring(0, _baseDirectory.Length - 1);
			return _baseDirectory;
		}

		public static string FullPath(string relativePath) {
			var returnValue = Path.Combine(dataPath.Substring(0, dataPath.IndexOf("Assets")), relativePath);
			returnValue = ConvertPath(returnValue);
			return returnValue;
		}

		public static string RelativePath(string from, string to) {
			Uri uri1 = new Uri(Common.FullPath(from));
			Uri uri2 = new Uri(Common.FullPath(to));
			string relativePath = uri1.MakeRelativeUri(uri2).ToString();
			return ConvertPath(relativePath);
		}

		public static void DrawGUI(ref FastCompilationLibrary node, ref bool showAdvanced) {
			node.check[0] = GUILayout.Toggle(node.check[0], "Compile Runtime DLL");
			node.check[1] = GUILayout.Toggle(node.check[1], "Compile Editor DLL");

			if(node.checkPlugin == null || node.checkPlugin.Length < 3) {
				node.checkPlugin = new bool[3];
			}
			node.checkPlugin[0] = GUILayout.Toggle(node.checkPlugin[0], "Compile Plugin Runtime DLL");
			node.checkPlugin[1] = GUILayout.Toggle(node.checkPlugin[1], "Compile Plugin Editor DLL");

			node.syncScriptingDefineSymbols = GUILayout.Toggle(node.syncScriptingDefineSymbols, "Sync Unity's Scripting Define Symbols");

			GUILayout.Space(5);//https://forum.unity.com/threads/editor-gui-textfield-loses-focus.460929/
			GUILayout.Label("Additional Scripting Define Symbols (separated with ;)");
			
			GUI.SetNextControlName("additionalDefineSymbolsString");
			EditorGUI.BeginChangeCheck();
			node.additionalDefineSymbolsString = GUILayout.TextField(node.additionalDefineSymbolsString);
			if(EditorGUI.EndChangeCheck())
				GUI.changed = false;

			GUILayout.Space(5);
			
			showAdvanced = EditorGUILayout.Foldout(showAdvanced, "Advanced Settings (only edit if you know what you are doing)");
				
			if(showAdvanced) {
				#if SG_FUTURE
				if(GUILayout.Button("Convert CS Files in Library Directory")) {
					CompileToDll.Instance.ConvertDirectory(node);
					CompileToDll.UpdateNodeVSFiles(node);
				}	
				#endif
				GUILayout.Space(1);
				
				node.check[2] = GUILayout.Toggle(node.check[2], "Compile Runtime DLL (used in editor)");
				node.checkPlugin[2] = GUILayout.Toggle(node.checkPlugin[2], "Compile Plugin Runtime DLL (used in editor)");

				if(node.dllGUIDs.Length < 2)
					node.dllGUIDs = new string[2];
				if(node.pluginDLLGUIDs.Length < 2)
					node.pluginDLLGUIDs = new string[2];

				bool runtimeDLLEnabled = node.check[0] || node.check[2];
				bool editorDLLEnabled = node.check[1];

				bool pluginRuntimeDLLEnabled = node.checkPlugin[0] || node.checkPlugin[2];
				bool pluginEditorDLLEnabled = node.checkPlugin[1];

				GUI.backgroundColor = Color.white;

				GUILayout.Space(1);
				if(!runtimeDLLEnabled)
					GUI.backgroundColor = Color.white * 0.5f;
				GUILayout.Label("Runtime DLL GUID");
				GUILayout.BeginHorizontal();
				node.dllGUIDs[0] = GUILayout.TextArea(node.dllGUIDs[0]);
				if(GUILayout.Button("Get from .meta file") && runtimeDLLEnabled) {
					node.dllGUIDs[0] = ChooseGUIDFromMeta(node, node.dllGUIDs[0]);
				}
				GUILayout.EndHorizontal();
				GUI.backgroundColor = Color.white;

				if(!editorDLLEnabled)
					GUI.backgroundColor = Color.white * 0.5f;
				GUILayout.Label("Editor DLL GUID");
				GUILayout.BeginHorizontal();
				node.dllGUIDs[1] = GUILayout.TextArea(node.dllGUIDs[1]);
				if(GUILayout.Button("Get from .meta file") && editorDLLEnabled) {
					node.dllGUIDs[1] = ChooseGUIDFromMeta(node, node.dllGUIDs[1]);
				}
				GUILayout.EndHorizontal();
				GUI.backgroundColor = Color.white;
                
				GUILayout.Space(1);
				if(!pluginRuntimeDLLEnabled)
					GUI.backgroundColor = Color.white * 0.5f;
				GUILayout.Label("Runtime Plugin DLL GUID");
				GUILayout.BeginHorizontal();
				node.pluginDLLGUIDs[0] = GUILayout.TextArea(node.pluginDLLGUIDs[0]);
				if(GUILayout.Button("Get from .meta file") && pluginRuntimeDLLEnabled) {
					node.pluginDLLGUIDs[0] = ChooseGUIDFromMeta(node, node.pluginDLLGUIDs[0]);
				}
				GUILayout.EndHorizontal();
				GUI.backgroundColor = Color.white;
				
				if(!pluginEditorDLLEnabled)
					GUI.backgroundColor = Color.white * 0.5f;
				GUILayout.Label("Editor Plugin DLL GUID");
				GUILayout.BeginHorizontal();
				node.pluginDLLGUIDs[1] = GUILayout.TextArea(node.pluginDLLGUIDs[1]);
				if(GUILayout.Button("Get from .meta file") && pluginEditorDLLEnabled) {
					node.pluginDLLGUIDs[1] = ChooseGUIDFromMeta(node, node.pluginDLLGUIDs[1]);
				}
				GUILayout.EndHorizontal();
				GUI.backgroundColor = Color.white;
			}

			if(focusedControl == "additionalDefineSymbolsString" && GUI.GetNameOfFocusedControl() != focusedControl)
				GUI.changed = true;

			focusedControl = GUI.GetNameOfFocusedControl();

			if (focusedControl == "additionalDefineSymbolsString" && Event.current.keyCode == KeyCode.Return) {
				GUI.changed = true;
			}
		}

		static public string ChooseGUIDFromMeta(FastCompilationLibrary node, string defaultGUID) {
			string metaFile = EditorUtility.OpenFilePanelWithFilters("Select meta file to get GUID from", node.baseDirectory, new string[] { "Meta Files", "meta" });
			if(!string.IsNullOrEmpty(metaFile)) {
				string contents;
				var guids = CompileToDll.GetGuidsFromMetaFile(metaFile, out contents);
				return guids.First();
			}
			return defaultGUID;
		}

		public static void OpenSolution(TreeViewItem obj)
		{
			var node = obj as TreeViewItem<FastCompilationLibrary>;
			var nodeData = node.data;
			OpenSolution(nodeData);
		}

		public static void SyncLibraryDefiningSymbols(FastCompilationLibrary fastCompilationLibrary, CSProjHelper xmlFile) {
			if(fastCompilationLibrary.syncScriptingDefineSymbols) {
				foreach (string option in UnityEditor.EditorUserBuildSettings.activeScriptCompilationDefines) {
					xmlFile.AddDefineConstants (option);
				}
			}

			foreach (string option in fastCompilationLibrary.additionalDefineSymbols) {
				xmlFile.AddDefineConstants (option);
			}
		}

		public static void OpenSolution(FastCompilationLibrary nodeData) {
			CompileToDll.UpdateNodeVSFiles(nodeData);

			System.Diagnostics.Process.Start(nodeData.solutionSubDir);
		}

		public static void FixScriptingDefines(FastCompilationLibrary nodeData) {
			if(File.Exists(nodeData.runtimeProjSubDir)) {
				var xmlFile = new CSProjHelper (nodeData.runtimeProjSubDir, false);
				SyncLibraryDefiningSymbols(nodeData, xmlFile);
				xmlFile.Save();
			}

			if(File.Exists(nodeData.editorProjSubDir)) {
				var xmlFile = new CSProjHelper (nodeData.editorProjSubDir, true);
				xmlFile.AddDefineConstants ("UNITY_EDITOR");
				SyncLibraryDefiningSymbols(nodeData, xmlFile);
				xmlFile.Save();
			}

			if(File.Exists(nodeData.pluginsRuntimeProjSubDir)) {
				var xmlFile = new CSProjHelper (nodeData.pluginsRuntimeProjSubDir, false);
				SyncLibraryDefiningSymbols(nodeData, xmlFile);
				xmlFile.Save();
			}

			if(File.Exists(nodeData.pluginsEditorProjSubDir)) {
				var xmlFile = new CSProjHelper (nodeData.pluginsEditorProjSubDir, true);
				xmlFile.AddDefineConstants ("UNITY_EDITOR");
				SyncLibraryDefiningSymbols(nodeData, xmlFile);
				xmlFile.Save();
			}
		}
	}
}
