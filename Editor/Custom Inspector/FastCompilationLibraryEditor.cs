﻿// Copyright (c) Stranger Games http://www.stranger-games.com/ . All rights reserved.
// Licensed under the GPLv2 license. See LICENSE file in the project root.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.IMGUI.Controls;

namespace StrangerGames.FastCompile {
	[CustomEditor(typeof(FastCompilationLibrary))]
	public class FastCompilationLibraryEditor : Editor {

		FastCompilationLibrary node{
			get{
				return (FastCompilationLibrary)target;
			}
		}

		public override void OnInspectorGUI()
		{
			if(GUILayout.Button("Libraries Manager Window"))
			{
				NodeTree.GetWindow();
			}
		}

		void Awake()
		{
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}
	}
}
