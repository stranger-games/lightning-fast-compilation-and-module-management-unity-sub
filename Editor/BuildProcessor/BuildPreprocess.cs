﻿// Copyright (c) Stranger Games http://www.stranger-games.com/ . All rights reserved.
// Licensed under the GPLv2 license. See LICENSE file in the project root.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Build;
using System;
using UnityEditor;
using System.IO;

namespace StrangerGames.FastCompile {
	public class BuildPreprocess : IPreprocessBuild {
		public int callbackOrder {
			get {
				return 0;
			}
		}

		public void OnPreprocessBuild(BuildTarget target, string path) {
			var libraries = CompileToDll.GetCompileLibrariesThatNeedsBuildProcessing();
			foreach(var library in libraries) {
				CompileToDll.BuildProcessing(library, false, CompileToDll.OutputDLLType.RUNTIME_EDITOR, true);
				CompileToDll.BuildProcessing(library, false, CompileToDll.OutputDLLType.RUNTIME);
			}

			libraries = CompileToDll.GetCompileLibrariesThatNeedsPluginBuildProcessing();
			foreach(var library in libraries) {
				CompileToDll.BuildProcessing(library, true, CompileToDll.OutputDLLType.RUNTIME_EDITOR, true);
				CompileToDll.BuildProcessing(library, true, CompileToDll.OutputDLLType.RUNTIME);
			}
		}
	}
}
