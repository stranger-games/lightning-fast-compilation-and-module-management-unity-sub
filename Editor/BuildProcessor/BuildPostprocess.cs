﻿// Copyright (c) Stranger Games http://www.stranger-games.com/ . All rights reserved.
// Licensed under the GPLv2 license. See LICENSE file in the project root.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Build;
using System;
using UnityEditor;

namespace StrangerGames.FastCompile {
	public class BuildPostprocess : IPostprocessBuild {
		public int callbackOrder {
			get {
				return 0;
			}
		}

		public void OnPostprocessBuild(BuildTarget target, string path) {
			var libraries = CompileToDll.GetCompileLibrariesThatNeedsBuildProcessing();
			foreach(var library in libraries) {
				CompileToDll.BuildProcessing(library, false, CompileToDll.OutputDLLType.RUNTIME, true);
				CompileToDll.BuildProcessing(library, false, CompileToDll.OutputDLLType.RUNTIME_EDITOR);
			}

			libraries = CompileToDll.GetCompileLibrariesThatNeedsPluginBuildProcessing();
			foreach(var library in libraries) {
				CompileToDll.BuildProcessing(library, true, CompileToDll.OutputDLLType.RUNTIME, true);
				CompileToDll.BuildProcessing(library, true, CompileToDll.OutputDLLType.RUNTIME_EDITOR);
			}
		}
	}
}
