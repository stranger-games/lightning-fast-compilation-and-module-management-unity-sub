﻿// Copyright (c) Stranger Games http://www.stranger-games.com/ . All rights reserved.
// Licensed under the GPLv2 license. See LICENSE file in the project root.

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Random = UnityEngine.Random;
using UnityEditor.TreeViewExamples;
using System.IO;
using StrangerGames.FastCompile;
using StrangerGames.FastCompile.TreeViewExamples;

namespace StrangerGames.FastCompile {
	[Serializable]
	public class FastCompilationLibrary : TreeElement {

		[SerializeField]
		public bool[] check = new bool[3];
		[SerializeField]
		public bool[] checkPlugin = new bool[3];
	
		[SerializeField]
		public string[] dllGUIDs = new string[3];
		[SerializeField]
		public string[] pluginDLLGUIDs = new string[3];

		[SerializeField]
		public bool syncScriptingDefineSymbols = false;
    
		[SerializeField]
		public List<FastCompilationLibrary> depedency = new List<FastCompilationLibrary>();
	
		[SerializeField]
		public string additionalDefineSymbolsString = "";

		public List<string> additionalDefineSymbols {
			get {
				string[] arrayDefineSysmbol = additionalDefineSymbolsString.Split(';');                    
				List<string> returnValue = new List<string>();

				for (int i = 0; i < arrayDefineSysmbol.Length; i++)
				{
					if(!string.IsNullOrEmpty(arrayDefineSysmbol[i])){
						returnValue.Add(arrayDefineSysmbol[i]);
					}
				}

				return returnValue;
			}
		}

		public bool DebugBuild {
			get {
				return additionalDefineSymbols.Contains("Debug") || additionalDefineSymbols.Contains("debug") || additionalDefineSymbols.Contains("DEBUG");
			}
		}

		[SerializeField]
		string directory;
		[SerializeField]
		string pluginOutputDirectory;

		public static string ShortenPath(string path) {
			if(string.IsNullOrEmpty(path))
				return path;
			int index = path.IndexOf("Assets" + Path.DirectorySeparatorChar);
			if(index == -1)
				return path;
			return path.Substring(index);
		}

		public string baseDirectory {
			get {
				return Common.ConvertPath(directory);
			}
			set {
				directory = Common.ConvertPath(value);
				directory = ShortenPath(directory);
			}
		}

		public string pluginOutputBaseDirectory {
			get {
				return Common.ConvertPath(pluginOutputDirectory);
			}
			set {
				pluginOutputDirectory = Common.ConvertPath(value);
				pluginOutputDirectory = ShortenPath(pluginOutputDirectory);
			}
		}

		[SerializeField]
		public bool pluginManaged;

		[SerializeField]
		string _solutionSubDir;
		[SerializeField]
		string _runtimeProjSubDir;
		[SerializeField]
		string _editorProjSubDir;
		[SerializeField]
		string _pluginsRuntimeProjSubDir;
		[SerializeField]
		string _pluginsEditorProjSubDir;

		public string solutionSubDir {
			get {
				if(string.IsNullOrEmpty(_solutionSubDir)) {
					return baseSolutionProjSubDir(VSFilesBaseDirectory);
				}
				return Common.ConvertPath(_solutionSubDir);
			}
			set {
				_solutionSubDir = Common.ConvertPath(value);
				_solutionSubDir = ShortenPath(_solutionSubDir);
			}
		}

		public string runtimeProjSubDir  {
			get {
				if(string.IsNullOrEmpty(_runtimeProjSubDir)) {
					return baseRuntimeProjSubDir(VSFilesBaseDirectory);
				}
				return Common.ConvertPath(_runtimeProjSubDir);
			}
			set {
				_runtimeProjSubDir = Common.ConvertPath(value);
				_runtimeProjSubDir = ShortenPath(_runtimeProjSubDir);
			}
		}

		public string editorProjSubDir {
			get {
				if(string.IsNullOrEmpty(_editorProjSubDir)) {
					return baseEditorProjSubDir(VSFilesBaseDirectory);
				}
				return Common.ConvertPath(_editorProjSubDir);
			}
			set {
				_editorProjSubDir = Common.ConvertPath(value);
				_editorProjSubDir = ShortenPath(_editorProjSubDir);
			}
		}

		public string pluginsRuntimeProjSubDir
		{
			get
			{            
				if(string.IsNullOrEmpty(_pluginsRuntimeProjSubDir))
					return baseRuntimeProjSubDir(VSFilesBaseDirectory, true);

				return Common.ConvertPath(_pluginsRuntimeProjSubDir);
			}
			set
			{
				_pluginsRuntimeProjSubDir = Common.ConvertPath(value);
				_pluginsRuntimeProjSubDir = ShortenPath(_pluginsRuntimeProjSubDir);
			}
		}

		public string pluginsEditorProjSubDir
		{
			get
			{
				if (string.IsNullOrEmpty(_pluginsEditorProjSubDir))
					return baseEditorProjSubDir(VSFilesBaseDirectory, true);

				return Common.ConvertPath(_pluginsEditorProjSubDir);
			}
			set
			{
				_pluginsEditorProjSubDir = Common.ConvertPath(value);
				_pluginsEditorProjSubDir = ShortenPath(_pluginsEditorProjSubDir);
			}
		}

		public string VSFilesBaseDirectory {
			get {
				return directory + Path.DirectorySeparatorChar + "VisualStudioFiles~";
			}
		}

		public string VSFilesBaseSrcDirectory {
			get {
				return Path.Combine(VSFilesBaseDirectory, "NonPlugins");
			}
		}

		public string VSFilesPluginSrcBaseDirectory {
			get {
				return Path.Combine(VSFilesBaseDirectory, "Plugins");
			}
		}

		public string VSFilesBaseRuntimePrjDirectory {
			get {
				return Path.Combine(VSFilesBaseSrcDirectory, "Runtime");
			}
		}

		public string VSFilesPluginRuntimePrjBaseDirectory {
			get {
				return Path.Combine(VSFilesPluginSrcBaseDirectory, "Runtime");
			}
		}

		public string VSFilesBaseEditorPrjDirectory {
			get {
				return Path.Combine(VSFilesBaseSrcDirectory, "Editor");
			}
		}

		public string VSFilesPluginEditorPrjBaseDirectory {
			get {
				return Path.Combine(VSFilesPluginSrcBaseDirectory, "Editor");
			}
		}

		public static string baseSolutionProjSubDir(string directory)
		{
			string filename = "FastCompileDLL.sln";

			if (string.IsNullOrEmpty(directory))
				return Path.DirectorySeparatorChar + "FastCompileDLL.sln";

			return directory + Path.DirectorySeparatorChar + filename;
		}

		public string baseRuntimeProjSubDir(string directory, bool plugin = false)
		{
			string filename = "FastCompileDLL.csproj";
			if(plugin) {
				filename = "FastCompilePluginDLL.csproj";
			}

			if(string.IsNullOrEmpty(directory))
				return Path.DirectorySeparatorChar + filename;
			
			string baseDir;
			if(plugin) {
				baseDir = VSFilesPluginRuntimePrjBaseDirectory;
			}
			else {
				baseDir = VSFilesBaseRuntimePrjDirectory;
			}

			return baseDir + Path.DirectorySeparatorChar + filename;
		}

		public string baseEditorProjSubDir(string directory, bool plugin = false)
		{
			string filename = "FastCompileDLLEditor.csproj";
			if(plugin) {
				filename = "FastCompilePluginDLLEditor.csproj";
			}

			if (string.IsNullOrEmpty(directory))
				return Path.DirectorySeparatorChar + filename;
		
			string baseDir;
			if(plugin) {
				baseDir = VSFilesPluginEditorPrjBaseDirectory;
			}
			else {
				baseDir = VSFilesBaseEditorPrjDirectory;
			}

			return baseDir + Path.DirectorySeparatorChar + filename;
		}

		public FastCompilationLibrary (string name, int depth, string pDirectory = "", string pPluginDirectory = "", bool pPluginManaged = true) : base (name, depth)
		{
			if(check == null || check.Length < 3)
				check = new bool[3];
			for (int i = 0; i < check.Length; i++)
			{
				check[i] = true;
			}
			check[1] = false;
			check[2] = false;

			if(checkPlugin == null || checkPlugin.Length < 3)
				checkPlugin = new bool[3];
			for (int i = 0; i < checkPlugin.Length; i++)
			{
				checkPlugin[i] = true;
			}
			checkPlugin[0] = false;
			checkPlugin[1] = false;
			checkPlugin[2] = false;

			InitGUIDS();

			baseDirectory = pDirectory;
			pluginOutputBaseDirectory = pPluginDirectory;

			pluginManaged = pPluginManaged;
		}

		private void Awake() {
			InitGUIDS();
		}

		void InitGUIDS() {
			if(dllGUIDs == null || dllGUIDs.Length < 2)
				dllGUIDs = new string[2];
			for (int i = 0; i < dllGUIDs.Length; i++)
			{
				if(string.IsNullOrEmpty(dllGUIDs[i]))
					dllGUIDs[i] = Guid.NewGuid().ToString("N");
			}

			if(pluginDLLGUIDs == null || pluginDLLGUIDs.Length < 2)
				pluginDLLGUIDs = new string[2];
			for (int i = 0; i < pluginDLLGUIDs.Length; i++)
			{
				if(string.IsNullOrEmpty(pluginDLLGUIDs[i]))
					pluginDLLGUIDs[i] = Guid.NewGuid().ToString("N");
			}
		}

		public static FastCompilationLibrary CreateNewOrLoadLibrary(string libraryName, string baseDirectory = "", string pluginDirectory = "", bool pluginManaged = true) {
			FastCompilationLibrary newNode = null;
			var relativeNormalDirectory = FastCompilationLibrary.ShortenPath(Common.ConvertPath(baseDirectory));
			var assetPaths = AssetDatabase.FindAssets("t:FastCompilationLibrary", new string[] {relativeNormalDirectory});
			string assetPath = null;
				
			if(assetPaths.Length > 0)
				assetPath = assetPaths[0];

			if (!string.IsNullOrEmpty(assetPath))
				newNode = AssetDatabase.LoadAssetAtPath<FastCompilationLibrary>(assetPath);
				
			if(newNode == null) {
				newNode = new FastCompilationLibrary(libraryName, 0, baseDirectory, pluginDirectory, pluginManaged);
			}

			newNode.name = libraryName;

			AssetDatabase.CreateAsset(newNode, relativeNormalDirectory + Path.DirectorySeparatorChar + libraryName + ".asset");

			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();

			return newNode;
		}
	}
}
