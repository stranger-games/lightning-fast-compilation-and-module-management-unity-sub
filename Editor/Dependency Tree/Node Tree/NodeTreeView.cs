﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.IMGUI.Controls;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEditor.TreeViewExamples;
using UnityEditor;
using StrangerGames.FastCompile.TreeViewExamples;

namespace StrangerGames.FastCompile {
	internal class NodeTreeView : TreeViewWithTreeModel<FastCompilationLibrary> {

		const float kRowHeights = 20f;
		const float kToggleWidth = 18f;
		public static event Action<TreeViewItem> GetFunction;
		//public bool showControls = true;
		TreeViewItem selectedItem;

		enum MyColumns
		{
			//CheckBox,
			Name,
			Button,
		
		}

		public void TreeToList (IList<TreeViewItem> result)
		{
			TreeViewItem root = rootItem;

			if (root == null)
				throw new NullReferenceException("root");
			if (result == null)
				throw new NullReferenceException("result");

			result.Clear();

			if (root.children == null)
				return;

			Stack<TreeViewItem> stack = new Stack<TreeViewItem>();
			for (int i = root.children.Count - 1; i >= 0; i--)
				stack.Push(root.children[i]);

			while (stack.Count > 0)
			{
				TreeViewItem current = stack.Pop();
				result.Add(current);

				if (current.hasChildren && current.children[0] != null)
				{
					for (int i = current.children.Count - 1; i >= 0; i--)
					{
						stack.Push(current.children[i]);
					}
				}
			}
		}

		public TreeViewItem GetSelected(){

			if (selectedItem == null || !IsSelected(selectedItem.id))
			{            
				foreach (TreeViewItem item in GetRows())
				{

					if (IsSelected(item.id))
					{
						selectedItem = item;
						break;
					}

				}
			}

			return selectedItem;

		}

		public TreeViewItem[] GetSelecteds(){

			if(GetSelection().Count < 1) return null;

			TreeViewItem[] selected = new TreeViewItem[GetSelection().Count];

			for (int i = 0; i < GetSelection().Count; i++)
			{
				foreach(TreeViewItem item in rootItem.children){
			
					if(item.id == GetSelection()[i]){
						selected[i] = item;
						break;
					}

				}
			}        

			return selected;

		}

		public NodeTreeView (TreeViewState state, MultiColumnHeader multicolumnHeader, IList<FastCompilationLibrary> data) : base (state, multicolumnHeader, data)
		{
			Assert.AreEqual(2 , Enum.GetValues(typeof(MyColumns)).Length, "Ensure number of sort options are in sync with number of MyColumns enum values");

			// Custom setup
			rowHeight = kRowHeights;
			columnIndexForTreeFoldouts = 0;
			showAlternatingRowBackgrounds = false;
			showBorder = false;
			customFoldoutYOffset = (kRowHeights - EditorGUIUtility.singleLineHeight) * 0.5f; // center foldout in the row since we also center content. See RowGUI
			extraSpaceBeforeIconAndLabel = kToggleWidth;
			//multicolumnHeader.sortingChanged += OnSortingChanged;
		
			Reload();
		}

		protected override bool CanStartDrag (CanStartDragArgs args)
		{
			return false;
		}

		public override void OnDropDraggedElementsAtIndex (List<TreeViewItem> draggedRows, FastCompilationLibrary parent, int insertIndex)
		{
			bool isFromOutside = true;

			foreach (TreeViewItem<FastCompilationLibrary> x in draggedRows){
				if(Find(x.data.id) == x.data){
					isFromOutside = false;
					break;
				}
			}

			if(isFromOutside){
				List<FastCompilationLibrary> draggedElements = new List<FastCompilationLibrary>();
				int add = GenerateUniqueID();
				foreach (TreeViewItem<FastCompilationLibrary> x in draggedRows)
				{
					FastCompilationLibrary newNode = new FastCompilationLibrary(x.data.name, 0);
					draggedElements.Add(newNode);
					add++;
				}

				var selectedIDs = draggedElements.Select(x => x.id).ToArray();
				AddElements(draggedElements, parent, insertIndex);
				SetSelection(selectedIDs, TreeViewSelectionOptions.RevealAndFrame);
			}
			else
				base.OnDropDraggedElementsAtIndex(draggedRows, parent, insertIndex);
            
		}

		protected override DragAndDropVisualMode HandleDragAndDrop(DragAndDropArgs args)
		{
			// Check if we can handle the current drag data (could be dragged in from other areas/windows in the editor)
			var draggedRows = DragAndDrop.GetGenericData(k_GenericDragID) as List<TreeViewItem>;
			if (draggedRows == null)
				return DragAndDropVisualMode.None;

			// Parent item is null when dragging outside any tree view items.
			switch (args.dragAndDropPosition)
			{
				case DragAndDropPosition.UponItem:
				case DragAndDropPosition.BetweenItems:
					{
						bool validDrag = ValidDrag(args.parentItem, draggedRows);
						if (args.performDrop && validDrag)
						{
							GetNewData(draggedRows);
						}
						return validDrag ? DragAndDropVisualMode.Move : DragAndDropVisualMode.None;
					}

				case DragAndDropPosition.OutsideItems:
					{
						if (args.performDrop)
						{
							GetNewData(draggedRows);
						}      
						return DragAndDropVisualMode.Move;
					}
				default:
					Debug.LogError("Unhandled enum " + args.dragAndDropPosition);
					return DragAndDropVisualMode.None;
			}
		}

		protected virtual void GetNewData(List<TreeViewItem> draggedRows){

			int add = GenerateUniqueID();
			foreach (TreeViewItem<FastCompilationLibrary> x in draggedRows)
			{
				FastCompilationLibrary newNode = x.data;
				NodeTree.GetWindow().AddTableItemToDepedency(newNode);
				// draggedElements.Add(newNode);
				add++;        
			}

		}

		// Note we We only build the visible rows, only the backend has the full tree information. 
		// The treeview only creates info for the row list.
		protected override IList<TreeViewItem> BuildRows(TreeViewItem root)
		{
			var rows = base.BuildRows (root);		
			return rows;
		}

		protected override void RowGUI (RowGUIArgs args)
		{
			var item = (TreeViewItem<FastCompilationLibrary>) args.item;

			for (int i = 0; i < args.GetNumVisibleColumns (); ++i)
			{
				CellGUI(args.GetCellRect(i), item, (MyColumns)args.GetColumn(i), ref args);
			}
		}

		void CellGUI (Rect cellRect, TreeViewItem<FastCompilationLibrary> item, MyColumns column, ref RowGUIArgs args)
		{
			// Center cell rect vertically (makes it easier to place controls, icons etc in the cells)
			CenterRectUsingSingleLineHeight(ref cellRect);

			switch (column)
			{

			case MyColumns.Name:
				{
					// Do toggle
					/* Rect toggleRect = cellRect;
					toggleRect.x += GetContentIndent(item);
					toggleRect.width = kToggleWidth;
					if (toggleRect.xMax < cellRect.xMax)
						item.data.enabled = EditorGUI.Toggle(toggleRect, item.data.enabled); // hide when outside cell rect */

					// Default icon and label
					args.rowRect = cellRect;
					base.RowGUI(args);
				}
				break;

				case MyColumns.Button:
				{
					cellRect.x += GetContentIndent(item);
					cellRect.width = 100;				
					#if SG_FUTURE
					if(GUI.Button(cellRect, "Get")){
						if(GetFunction!=null)
							GetFunction(item);
					}
					#endif
				}
				break;
			}
		}	

		public static MultiColumnHeaderState CreateDefaultMultiColumnHeaderState()
		{
			var columns = new[]
			{
				new MultiColumnHeaderState.Column
				{
					headerContent = new GUIContent("Depedency List"),
					headerTextAlignment = TextAlignment.Center,
					width = 150,
					minWidth = 60,
					autoResize = false,
					allowToggleVisibility = false
				},
				new MultiColumnHeaderState.Column
				{
					width = 150,
					minWidth = 100,
					autoResize = false,
        			allowToggleVisibility = false
				},
			
			};

			Assert.AreEqual(columns.Length, Enum.GetValues(typeof(MyColumns)).Length, "Number of columns should match number of enum values: You probably forgot to update one of them.");

			var state =  new MultiColumnHeaderState(columns);
			return state;
		}
	}
}
