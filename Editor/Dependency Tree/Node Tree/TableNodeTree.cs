﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.IMGUI.Controls;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEditor.TreeViewExamples;
using UnityEditor;
using StrangerGames.FastCompile.TreeViewExamples;

namespace StrangerGames.FastCompile {
	internal class TableNodeTree : TreeViewWithTreeModel<FastCompilationLibrary> {

		const float kRowHeights = 35f;
		const float kToggleWidth = 18f;
		int lastSelectedId = -1;
		public static event Action<TreeViewItem<FastCompilationLibrary>> FirstFunction;
		public static event Action<TreeViewItem> OtherFunction;
		//public bool showControls = true;
		TreeViewItem selectedItem;

		enum MyColumns
		{
			//CheckBox,
			Name,
			Button,
			UpdateSolution,
			Third,
			CompileSolution,
			ProjectFolder,
		}

		public TreeViewItem GetSelected(){

			if (selectedItem == null || !IsSelected(selectedItem.id))
			{            
				foreach (TreeViewItem item in GetRows())
				{

					if (IsSelected(item.id))
					{
						selectedItem = item;
						break;
					}

				}
			}

			return selectedItem;

		}

		public FastCompilationLibrary GetSelectedNode()
		{
			if(GetSelected() == null)
				return null;
        
			return Find(GetSelected().id);
		}

		public FastCompilationLibrary FindNode(int id){
			return Find(id);
		}

		public bool SelectedChange
		{
			get
			{
				if(GetSelected() == null) return false;

				bool change = lastSelectedId != GetSelected().id;

				if (change)
					lastSelectedId = GetSelected().id;

				return change;
			}
		}

		public TableNodeTree (TreeViewState state, MultiColumnHeader multicolumnHeader, IList<FastCompilationLibrary> data) : base (state, multicolumnHeader, data)
		{
			Assert.AreEqual(6 , Enum.GetValues(typeof(MyColumns)).Length, "Ensure number of sort options are in sync with number of MyColumns enum values");

			// Custom setup
			rowHeight = kRowHeights;
			columnIndexForTreeFoldouts = 0;
			showAlternatingRowBackgrounds = false;
			showBorder = false;
			customFoldoutYOffset = (kRowHeights - EditorGUIUtility.singleLineHeight) * 0.5f; // center foldout in the row since we also center content. See RowGUI
			extraSpaceBeforeIconAndLabel = kToggleWidth;
			//multicolumnHeader.sortingChanged += OnSortingChanged;
		
			Reload();
		}

		/* protected override bool CanStartDrag (CanStartDragArgs args)
		{
			return false;
		} */

		public override void OnDropDraggedElementsAtIndex (List<TreeViewItem> draggedRows, FastCompilationLibrary parent, int insertIndex)
		{		
		
		}

		// Note we We only build the visible rows, only the backend has the full tree information. 
		// The treeview only creates info for the row list.
		protected override IList<TreeViewItem> BuildRows(TreeViewItem root)
		{
			var rows = base.BuildRows (root);		
			return rows;
		}

		protected override void RowGUI (RowGUIArgs args)
		{
			var item = (TreeViewItem<FastCompilationLibrary>) args.item;

			for (int i = 0; i < args.GetNumVisibleColumns (); ++i)
			{
				CellGUI(args.GetCellRect(i), item, (MyColumns)args.GetColumn(i), ref args);
			}
		}

		void OpenFolder(object path) {
			System.Diagnostics.Process.Start((string)path);
		}

		void AddOpenFolderMenuItem(GenericMenu menu, string menuPath, string path, bool enabled)
		{
			if(enabled) {
				// the menu item is marked as selected if it matches the current value of m_Color
				menu.AddItem(new GUIContent(menuPath), false, OpenFolder, path);
			}
		}

		void CellGUI (Rect cellRect, TreeViewItem<FastCompilationLibrary> item, MyColumns column, ref RowGUIArgs args)
		{
			// Center cell rect vertically (makes it easier to place controls, icons etc in the cells)
			CenterRectUsingSingleLineHeight(ref cellRect);
			cellRect.height = 30;
			cellRect.y -= 8;

			switch (column)
			{

				case MyColumns.Name:
				{
					// Do toggle
					Rect toggleRect;				

					/* for (int i = 0; i < item.data.check.Length; i++)
					{
						toggleRect = cellRect;
						toggleRect.x += GetContentIndent(item)+ kToggleWidth * i;
						toggleRect.width = kToggleWidth;

						if (toggleRect.xMax < cellRect.xMax)
							item.data.check[i] = EditorGUI.Toggle(toggleRect, item.data.check[i]); // hide when outside cell rect
					} */

					toggleRect = cellRect;
					toggleRect.x = -10;//GetContentIndent(item);// + kToggleWidth;// * item.data.check.Length;				
				
					// Default icon and label
					args.rowRect = toggleRect;
					base.RowGUI(args);
				}
				break;

				case MyColumns.Button:
				{
					cellRect.x += GetContentIndent(item);
					cellRect.width = 100;				

					if(GUI.Button(cellRect, "Add as\ndependency")){
					if(FirstFunction != null)
						FirstFunction(item);
					}
				}
				break;

				case MyColumns.UpdateSolution:
				{				
					cellRect.x += GetContentIndent(item);
					cellRect.width = 60;

					if (GUI.Button(cellRect, "Update\nSolution"))
					{
						CompileToDll.UpdateNodeVSFiles(item.data);
					}
				}
				break;

				case MyColumns.Third:
				{
					cellRect.x += GetContentIndent(item);
					cellRect.width = 60;

					if(GUI.Button(cellRect, "Open\nSolution")){
						if(OtherFunction != null)
							OtherFunction(item);
					}
				}
				break;

				case MyColumns.CompileSolution:
				{
					cellRect.x += GetContentIndent(item);
					cellRect.width = 60;

					if (GUI.Button(cellRect, "Compile\nSolution"))
					{
						CompileToDll.StartCompiling(item.data);
					}
				}
				break;

				case MyColumns.ProjectFolder:
				{
					cellRect.x += GetContentIndent(item);
					cellRect.width = 60;

					if (GUI.Button(cellRect, "Project\nFolder"))
					{
						GenericMenu menu = new GenericMenu();

						// forward slashes nest menu items under submenus
						AddOpenFolderMenuItem(menu, "Main Folder", item.data.baseDirectory, true);
					
						// an empty string will create a separator at the top level
						menu.AddSeparator("");

						AddOpenFolderMenuItem(menu, "Runtime Source Folder", item.data.VSFilesBaseRuntimePrjDirectory, item.data.check[0] || item.data.check[2]);
						AddOpenFolderMenuItem(menu, "Editor Source Folder", item.data.VSFilesBaseEditorPrjDirectory, item.data.check[1]);
					
						AddOpenFolderMenuItem(menu, "Plugin Runtime Source Folder", item.data.VSFilesPluginRuntimePrjBaseDirectory, item.data.checkPlugin[0] || item.data.checkPlugin[2]);
						AddOpenFolderMenuItem(menu, "Plugin Editor Source Folder", item.data.VSFilesPluginEditorPrjBaseDirectory, item.data.checkPlugin[1]);

						// display the menu
						menu.ShowAsContext();
					}
				}
				break;
			}
		}
		protected override bool CanRename(TreeViewItem item)
		{
			// Only allow rename if we can show the rename overlay with a certain width (label might be clipped by other columns)
			Rect renameRect = GetRenameRect (treeViewRect, 0, item);
			return renameRect.width > 30;
		}

		protected override void RenameEnded(RenameEndedArgs args)
		{
			// Set the backend name and reload the tree to reflect the new model
			if (args.acceptedRename)
			{
				var element = Find(args.itemID);
				element.name = args.newName;
				Reload();
			}
		}

		protected override Rect GetRenameRect (Rect rowRect, int row, TreeViewItem item)
		{
			Rect cellRect = GetCellRectForTreeFoldouts (rowRect);
			CenterRectUsingSingleLineHeight(ref cellRect);
			return base.GetRenameRect (cellRect, row, item);
		}	

		public static MultiColumnHeaderState CreateDefaultMultiColumnHeaderState()
		{
			var columns = new[]
			{
				new MultiColumnHeaderState.Column
				{
					headerContent = new GUIContent("Name"),
					headerTextAlignment = TextAlignment.Center,
					width = 200,
					minWidth = 60,
					autoResize = false,
					allowToggleVisibility = false
				},
				new MultiColumnHeaderState.Column
				{
					width = 100,
					minWidth = 100,
					autoResize = false,
        			allowToggleVisibility = false
				},
				new MultiColumnHeaderState.Column
				{
					width = 60,
					minWidth = 60,
					autoResize = false,
					allowToggleVisibility = false
				},
				new MultiColumnHeaderState.Column
				{
					width = 60,
					minWidth = 60,
					autoResize = false,
        			allowToggleVisibility = false
				},
				new MultiColumnHeaderState.Column
				{
					width = 60,
					minWidth = 60,
					autoResize = false,
					allowToggleVisibility = false
				},
				new MultiColumnHeaderState.Column
				{
					width = 60,
					minWidth = 60,
					autoResize = false,
					allowToggleVisibility = false
				},
			};

			Assert.AreEqual(columns.Length, Enum.GetValues(typeof(MyColumns)).Length, "Number of columns should match number of enum values: You probably forgot to update one of them.");

			var state =  new MultiColumnHeaderState(columns);
			return state;
		}

	}
}
