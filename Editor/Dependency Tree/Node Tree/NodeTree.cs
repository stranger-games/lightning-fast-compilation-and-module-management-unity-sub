﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.IMGUI.Controls;
using UnityEditor.TreeViewExamples;
using StrangerGames.FastCompile;
using System.IO;
using System.Linq;
using StrangerGames.FastCompile.TreeViewExamples;

namespace StrangerGames.FastCompile {
	class NodeTree : EditorWindow {

		public static NodeTree Instance {get; private set; }
		public static bool IsOpen
		{
			get { return Instance != null; }
		}

		bool showAdvanced;

		void OnEnable()
		{
			Instance = this;
		}
		
		void OnDisable()
		{
			Instance = null;
		}

		void OnFocus()
		{
			GetBaseElementAssets();
		}

		[MenuItem ("Tools/Fast Compilation/Libraries List", false, 0)]
		public static NodeTree  GetWindow () {
			var window = GetWindow<NodeTree>();
                
			window.titleContent = new GUIContent("Fast Compilation Libraries List");
			window.Focus();
			window.Repaint();

			window.GetBaseElementAssets();

			return window;
		}

		List<FastCompilationLibrary> listElement = new List<FastCompilationLibrary>();

		public void GetBaseElementAssets()
		{
			string[] allGUID = AssetDatabase.FindAssets("t:FastCompilationLibrary");

			for (int i = 0; i < allGUID.Length; i++)
			{
				allGUID[i] = AssetDatabase.GUIDToAssetPath(allGUID[i]);
			}

			listElement.Clear();

			for (int i = 0; i < allGUID.Length; i++)
			{
				listElement.Add(AssetDatabase.LoadAssetAtPath<FastCompilationLibrary>(allGUID[i]));
			}

			m_InitializedTable = false;
		}

		[System.NonSerialized] bool m_InitializedTable;
		[System.NonSerialized] bool m_InitializedTree;

		// SerializeField is used to ensure the view state is written to the window 
		// layout file. This means that the state survives restarting Unity as long as the window
		// is not closed. If the attribute is omitted then the state is still serialized/deserialized.
		[SerializeField] TreeViewState m_TableViewState;
		[SerializeField] TreeViewState m_TreeViewState;
		[SerializeField] MultiColumnHeaderState m_MultiColumnHeaderState;

		//The TreeView is not serializable, so it should be reconstructed from the tree data.
		NodeTreeView m_TreeView;
		TableNodeTree m_TableTreeView;    
		public List<FastCompilationLibrary> listNodeTable = new List<FastCompilationLibrary>();
		public List<FastCompilationLibrary> listNodeTree = new List<FastCompilationLibrary>();

		float topMargin = 20;

		Vector2 scrollPos;   

		public TableNodeTree tableTreeView
		{
			get { return m_TableTreeView; }
		}

		public NodeTreeView treeView
		{
			get { return m_TreeView; }
		}

		/// <summary>
		/// OnGUI is called for rendering and handling GUI events.
		/// This function can be called multiple times per frame (one call per event).
		/// </summary>
		void OnGUI()
		{
			InitIfNeeded();
			ResetTree();

			scrollPos = EditorGUILayout.BeginScrollView(scrollPos);

			Rect rect = EditorGUILayout.BeginHorizontal();                
        
			CheckInput();        

			EditorGUILayout.BeginVertical();

			LeftSide();

			EditorGUILayout.EndVertical();        

			EditorGUILayout.BeginVertical(GUILayout.Width(150));
        
			RightSide();

			EditorGUILayout.EndVertical();

			EditorGUILayout.EndHorizontal();

			EditorGUILayout.EndScrollView();      

			if(GUI.changed){
				SaveTree();
			}  

		}       

		/// <summary>
		/// Called when the script is loaded or a value is changed in the
		/// inspector (Called in the editor only).
		/// </summary>
		void OnValidate()
		{
			GetBaseElementAssets();
			//SaveTree();
			//ResetTree();

		}

		//Rebuild The Table Tree in the top
		void InitIfNeeded ()
		{
			if (!m_InitializedTable)
			{
				// Check if it already exists (deserialized from window layout file or scriptable object)
				if (m_TableViewState == null)
					m_TableViewState = new TreeViewState();

				TableNodeTree.FirstFunction -= AddTableItemToTree;

				bool firstInit = m_MultiColumnHeaderState == null;
				var headerState = TableNodeTree.CreateDefaultMultiColumnHeaderState();
				if (MultiColumnHeaderState.CanOverwriteSerializedFields(m_MultiColumnHeaderState, headerState))
					MultiColumnHeaderState.OverwriteSerializedFields(m_MultiColumnHeaderState, headerState);
				m_MultiColumnHeaderState = headerState;
            
				var multiColumnHeader = new MultiColumnHeader(headerState);
				if (firstInit)
				   multiColumnHeader.ResizeToFit ();                    
            
				m_TableTreeView = new TableNodeTree(m_TableViewState, multiColumnHeader, GetTableData());
            
				TableNodeTree.FirstFunction += AddTableItemToTree;

				m_InitializedTable = true;
			}
		}

		IList<FastCompilationLibrary> GetTableData ()
		{
			//  if (m_MyTreeAsset != null && m_MyTreeAsset.treeElements != null && m_MyTreeAsset.treeElements.Count > 0)
			//      return m_MyTreeAsset.treeElements;

			listNodeTable.Clear();

			if (listNodeTable.Count == 0)
			{
				FastCompilationLibrary node = new FastCompilationLibrary("Root", -1);
				listNodeTable.Add(node);
			}

			for (int i = 0; i < listElement.Count; i++)
			{
				listElement[i].id = i + 1;
				listNodeTable.Add(listElement[i]);
			}

			return listNodeTable;
		}

		//Rebuild Simple Tree View in the bottom
		public void ResetTree(){

			if (!m_InitializedTree && m_TableTreeView.GetSelectedNode() != null)
			{
				// Check if it already exists (deserialized from window layout file or scriptable object)
				if (m_TreeViewState == null)
					m_TreeViewState = new TreeViewState();

				bool firstInit = m_MultiColumnHeaderState == null;
				var headerState = NodeTreeView.CreateDefaultMultiColumnHeaderState();
				if (MultiColumnHeaderState.CanOverwriteSerializedFields(m_MultiColumnHeaderState, headerState))
					MultiColumnHeaderState.OverwriteSerializedFields(m_MultiColumnHeaderState, headerState);
				m_MultiColumnHeaderState = headerState;
            
				var multiColumnHeader = new MultiColumnHeader(headerState);
				if (firstInit)
				   multiColumnHeader.ResizeToFit ();                    
            
				m_TreeView = new NodeTreeView(m_TreeViewState, multiColumnHeader, GetTreeData());            

				m_InitializedTree = true;
			}
		}

		IList<FastCompilationLibrary> GetTreeData ()
		{
			listNodeTree.Clear();

			FastCompilationLibrary node = new FastCompilationLibrary(listNodeTable[0].name, -1);
			listNodeTree.Add(node);

			if (tableTreeView != null && tableTreeView.GetSelectedNode() != null)
			{
				for (int i = 0; i < tableTreeView.GetSelectedNode().depedency.Count; i++)
				{
					node = tableTreeView.GetSelectedNode().depedency[i];
					listNodeTree.Add(node);
				}
			}
			return listNodeTree;
		}

		public void SaveTree()
		{
			string path;
			for (int i = 0; i < listElement.Count; i++)
			{
				if (listElement[i] != null)
				{
					path = AssetDatabase.GetAssetPath(listElement[i]);
					EditorUtility.SetDirty(listElement[i]);
					CompileToDll.UpdateNodeVSFiles(listElement[i]);
				}
			}      
		}

		string strNewData = "New Node";    

		public void LeftSide(){        

			Rect rect;

			GUILayout.BeginHorizontal();
			
			if(GUILayout.Button("Create New Library")){
            
				CompileToDll.CreateNew();
			}

			if (GUILayout.Button("Compile All"))
			{
				CompileToDll.StartCompiling();            
			}

			GUILayout.EndHorizontal();        

			if (m_TableTreeView != null)
			{
				rect = GUILayoutUtility.GetRect(0, 10000, 0, m_TableTreeView.totalHeight + 50);

				m_TableTreeView.OnGUI(rect);
			}
        
			if(m_TreeView != null)
			{            
				rect = GUILayoutUtility.GetRect (0, 10000, 0, m_TreeView.totalHeight+50);

				m_TreeView.OnGUI(rect);            
			}        

			if(tableTreeView.SelectedChange)
				m_InitializedTree = false;
		}

		public void Refresh() {
			GetBaseElementAssets();
			m_InitializedTable = false;        
		}

		public void RightSide(){    

			if (m_TreeView != null)
			{

				if (m_TreeView.GetSelection().Count == 1)
				{
					CheckSelection();
				}

			}

			if (m_TableTreeView != null)
			{
				if (m_TableTreeView.GetSelected() != null)
				{
					GUILayout.Label("Selected Node: " + m_TableTreeView.GetSelected().displayName);
					FastCompilationLibrary node = m_TableTreeView.GetSelectedNode();

					if(node == null) return;

					Common.DrawGUI(ref node, ref showAdvanced);

				}
				else
				{
					GUILayout.Label("Nothing Selected");
				}
			}   
               
		}

		void CheckInput()
		{
			Event e = Event.current;
			switch (e.type)
			{            
				case EventType.ValidateCommand:
					/* if(e.commandName == "Duplicate"){
						e.Use();
					} else */
					if(e.commandName == "SoftDelete"){
						e.Use();
					}
                
					break;

				case EventType.ExecuteCommand:
					/* if(e.commandName == "Duplicate"){                    
						DuplicateItem();
					} else */
					if(e.commandName == "SoftDelete"){
						DeleteItem();
					}
					break;    

				case EventType.ContextClick:
					GenericMenu menu = new GenericMenu();
                 
					//menu.AddItem(new GUIContent("Duplicate"), false, DuplicateItem);
					menu.AddItem(new GUIContent("Delete"), false, DeleteItem);
					menu.ShowAsContext();

					e.Use();
					break;                    
			}

		}

		void CheckSelection(){
        
			if(m_TreeView.GetSelected() == null) return;        

			List<int> selectedID = new List<int>();
			List<TreeViewItem> listItem = new List<TreeViewItem>();
        
			m_TreeView.TreeToList(listItem);
        
			for (int i = 0; i < listItem.Count; i++)
			{
				if(m_TreeView.GetSelected().displayName == listItem[i].displayName){
					m_TreeView.FrameItem(listItem[i].id);
					if (m_TreeView.GetSelected().id != listItem[i].id)
					{
						Texture2D texture = new Texture2D(1, 1);
						Color[] color = new Color[texture.width * texture.height];

						for (int j = 0; j < color.Length; j++)
						{
							color[j] = Color.green;
						}

						texture.SetPixels(color);
                    
						listItem[i].icon = texture;
					} else {
						listItem[i].icon = null;
					}
				} else {
					listItem[i].icon = null;
				}
			}            

		}

		void AddTableItemToTree(TreeViewItem<FastCompilationLibrary> item)
		{
			if(m_TableTreeView.GetSelectedNode() == null)
				Debug.Log("Selected node null");

			List<FastCompilationLibrary> listDepedency = m_TableTreeView.GetSelectedNode().depedency;
			FastCompilationLibrary newNode = item.data;

			bool canAdd = true;

			if (newNode.name == tableTreeView.GetSelectedNode().name)
				canAdd = false;

			if (canAdd)
				for (int i = 0; i < listDepedency.Count; i++)
				{
					if (listDepedency[i].name == newNode.name)
					{
						canAdd = false;
						break;
					}
				}

			if(canAdd)
				tableTreeView.GetSelectedNode().depedency.Add(newNode);
			// listNodeTree.Add(newNode);

			m_InitializedTree = false;
		}

		public void AddTableItemToDepedency(FastCompilationLibrary newNode)
		{
			List<FastCompilationLibrary> listDepedency = m_TableTreeView.GetSelectedNode().depedency;

			bool canAdd = true;

			if(newNode.name == tableTreeView.GetSelectedNode().name)
				canAdd = false;

			if(canAdd)
				for (int i = 0; i < listDepedency.Count; i++)
				{
					if (listDepedency[i].name == newNode.name)
					{                
						canAdd = false;
						break;
					}
				}

			if (canAdd)
			{            
				tableTreeView.GetSelectedNode().depedency.Add(newNode);
			}
			// listNodeTree.Add(newNode);

			m_InitializedTree = false;
		}

		void DuplicateItem(){

			if (m_TreeView.HasFocus())
			{
				if (m_TreeView.GetSelected() == null) return;

				TreeViewItem item = m_TreeView.GetSelected();

				FastCompilationLibrary newNode = new FastCompilationLibrary(item.displayName, 0);

				listNodeTree.Add(newNode);

				m_InitializedTree = false;
			} else if (m_TableTreeView.HasFocus())
			{
				if (m_TableTreeView.GetSelected() == null) return;

				TreeViewItem item = m_TableTreeView.GetSelected();

				FastCompilationLibrary newNode = new FastCompilationLibrary(item.displayName, 0);

				listNodeTable.Add(newNode);

				m_InitializedTable = false;
			}
		}

		void DeleteItem(){
			if (m_TreeView.HasFocus())
			{            
				if (m_TreeView.GetSelected() == null) return;

				List<FastCompilationLibrary> listDepedency = m_TableTreeView.GetSelectedNode().depedency;

				for (int i = 0; i < listDepedency.Count; i++)
				{
					if (listDepedency[i].id == m_TreeView.GetSelected().id)
					{                    
						m_TableTreeView.GetSelectedNode().depedency.RemoveAt(i);
						break;
					}
				}

				m_InitializedTree = false;
			} else if (m_TableTreeView.HasFocus())
			{
				if (m_TableTreeView.GetSelected() == null) return;

				for (int i = 0; i < listNodeTable.Count; i++)
				{
					if (listNodeTable[i].id == m_TableTreeView.GetSelected().id)
					{
						for (int j = 0; j < listNodeTree.Count; j++)
						{
							if(listNodeTree[j].name == listNodeTable[i].name){
								listNodeTree.RemoveAt(j);
								j--;
							}
						}
						AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(listNodeTable[i]));
						listNodeTable.RemoveAt(i);
						GetBaseElementAssets();
						break;
					}
				}
				m_InitializedTree = false;
				m_InitializedTable = false;
			}
		}
	
	}
}
