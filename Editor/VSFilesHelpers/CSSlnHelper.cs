﻿// Copyright (c) Stranger Games http://www.stranger-games.com/ . All rights reserved.
// Licensed under the GPLv2 license. See LICENSE file in the project root.

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using UnityEngine;

namespace StrangerGames.FastCompile {

	public class CSSlnHelper {

        public string projectID = "";

        XmlDocument document;
		public string slnFilename;

        string headerSearch = "HeaderTemplate";
        string mainSearch = "MainTemplate";
        string result;

        StreamReader reader;

        public CSSlnHelper(string filename, List<CSProjHelper> csProjects, string slnPath) {
			slnFilename = slnPath;
            projectID = "{"+System.Guid.NewGuid()+"}";

            // document = new XmlDocument();
            // document.Load(filename);

            reader = new StreamReader(filename);
            result = reader.ReadToEnd();
            string csprojName = "";

            string input = "";

            for (int i = 0; i < csProjects.Count; i++)
            {
				string relativePath = Common.RelativePath(slnPath, csProjects[i].csFilename);

                csprojName = Path.GetFileNameWithoutExtension(csProjects[i].csFilename);
                input += "Project(\""+projectID+"\") = \""+csprojName+"\", \"" + relativePath + "\", \""+csProjects[i].projectGUID+"\" \nEndProject";
                input += "\n";
            }            

            result = result.Replace(headerSearch, input);

            input = "";

            for (int i = 0; i < csProjects.Count; i++)
            {
                input += csProjects[i].projectGUID +".Debug|Any CPU.ActiveCfg = Debug|Any CPU\n"+
						csProjects[i].projectGUID +".Debug | Any CPU.Build.0 = Debug | Any CPU\n"+
						csProjects[i].projectGUID +".Release | Any CPU.ActiveCfg = Release | Any CPU\n"+
						csProjects[i].projectGUID +".Release | Any CPU.Build.0 = Release | Any CPU";
                input += "\n";
            }

            result = result.Replace(mainSearch, input);

            reader.Close();

        }

		public void Clear() {
			
		}

		public void Save(string filename) {
			slnFilename = filename;
			Save ();
		}

		public void Save() {

            if (File.Exists(slnFilename))
            {
                File.Delete(slnFilename);
            }
            using (StreamWriter writer = File.CreateText(slnFilename)/* new StreamWriter(slnFilename, true) */)
            {
                writer.Write(result);                   
            }
            
            Debug.Log("Done create sln: " + slnFilename);
        }
	}
}
