﻿// Copyright (c) Stranger Games http://www.stranger-games.com/ . All rights reserved.
// Licensed under the GPLv2 license. See LICENSE file in the project root.

using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using UnityEngine;

namespace StrangerGames.FastCompile {

	public class CSProjHelper {

		XmlDocument document;

		XmlNode configurationPropertyGroup;
		XmlNode debugPropertyGroup;
		XmlNode releasePropertyGroup;
		XmlNode referenceItemGroup;
		XmlNode compileItemGroup;

		XmlNode debugDefinesNode;
		XmlNode releaseDefinesNode;

		bool isEditorAssembly = false;
	
		public string csFilename;
        public string projectGUID = "";

        public string assemblyName {
			private set {
				_assemblyName = value;
			}
			get {
				if(_assemblyName != null)
					return _assemblyName;
				return configurationPropertyGroup["AssemblyName"].InnerText;
			}
		}

		string _assemblyName;

		public CSProjHelper(string filename, bool isEditor) {
			csFilename = filename;
			isEditorAssembly = isEditor;
            projectGUID = "{"+System.Guid.NewGuid()+"}";

            document = new XmlDocument ();
			document.Load (filename);

			var propNodes = document.GetElementsByTagName ("PropertyGroup");
			foreach (XmlNode prop in propNodes) {
				if (prop.Attributes != null && prop.Attributes.Count > 0 && prop.Attributes["Condition"] != null) {
					if (prop.Attributes["Condition"].Value.Contains ("Debug")) {
						debugPropertyGroup = prop;
						debugDefinesNode = debugPropertyGroup["DefineConstants"];
						continue;
					} else if (prop.Attributes["Condition"].Value.Contains ("Release")) {
						releasePropertyGroup = prop;
						releaseDefinesNode = releasePropertyGroup["DefineConstants"];
						continue;
					}
				}

				foreach (XmlNode subNode in prop.ChildNodes) {
					if (subNode.Name == "AssemblyName") {
						configurationPropertyGroup = prop;
						continue;
					}
				}
			}

			var itemGroupNodes = document.GetElementsByTagName ("ItemGroup");
			foreach (XmlNode itemNode in itemGroupNodes) {
				foreach (XmlNode subNode in itemNode.ChildNodes) {
					if (subNode.Name == "Reference") {
						referenceItemGroup = itemNode;
						continue;
					} else if (subNode.Name == "Compile") {
						compileItemGroup = itemNode;
						continue;
					}
				}
			}
		}

		public CSProjHelper(string filename){
            csFilename = filename;

            document = new XmlDocument();
            document.Load(filename);

            var propNodes = document.GetElementsByTagName("ProjectGuid");

			if(propNodes.Count == 0){
                projectGUID = "{"+System.Guid.NewGuid()+"}";                
			} else
            {
                projectGUID = propNodes.Item(0).InnerText;                
            }
		}

		XmlNode CreateUnityAssemblyNode (string name) {
			XmlNode node = document.CreateNode (XmlNodeType.Element, "Reference", "http://schemas.microsoft.com/developer/msbuild/2003");
			XmlAttribute includeAttr = document.CreateAttribute ("Include");
			includeAttr.Value = name;
			node.Attributes.Append (includeAttr);
			referenceItemGroup.AppendChild (node);
			return node;
		}

		public string OutputPath(bool release = true) {
			XmlNode propertyGroup = releasePropertyGroup;
			if (!release)
				propertyGroup = debugPropertyGroup;

			XmlNode outputPath = propertyGroup["OutputPath"];
			string relativePath = outputPath.InnerText;
			return Path.Combine (Path.GetDirectoryName (csFilename), Path.Combine (relativePath, assemblyName + ".dll"));
		}

		public void Clear() {
			compileItemGroup.RemoveAll ();
			referenceItemGroup.RemoveAll ();
		}

		public void ClearRefernceAssemblies() {
			referenceItemGroup.RemoveAll ();
		}

		public void SetAssemblyName(string newName) {
			XmlNode assemblyNameNode = configurationPropertyGroup["AssemblyName"];
			assemblyNameNode.InnerText = newName;
			assemblyName = newName;
		}

		public void SetLangVersion(string newVersion) {
			XmlNode assemblyNameNode = configurationPropertyGroup["LangVersion"];
			assemblyNameNode.InnerText = newVersion;
		}

		public void SetUnityVersion(string unityVersion) {
			XmlNode assemblyNameNode = configurationPropertyGroup["UnityVersion"];
			if(assemblyNameNode != null)
				assemblyNameNode.InnerText = unityVersion;
		}

		public void SetTargetFrameworkVersion(string targetFrameworkVersion) {
			XmlNode assemblyNameNode = configurationPropertyGroup["TargetFrameworkVersion"];
			assemblyNameNode.InnerText = targetFrameworkVersion;
		}

		public void Save(string filename) {
			csFilename = filename;
			Save ();
		}

		public void Save() {
			document.Save (csFilename);
		}

		public void AddSrcFile(string filename) {
			XmlNode node = document.CreateNode (XmlNodeType.Element, "Compile", "http://schemas.microsoft.com/developer/msbuild/2003");
			XmlAttribute includeAttr = document.CreateAttribute ("Include");
			includeAttr.Value = filename;
			node.Attributes.Append (includeAttr);
			compileItemGroup.AppendChild (node);
		}

		public void AddDefineConstants(string define, bool both = true, bool debug = false) {
			AddDefineConstants (new string[] { define }, both, debug);
		}

		public void AddDefineConstants(string[] defines, bool both = true, bool debug = false) {
			if(both || debug) {
				AppendDefineConstants (releaseDefinesNode, defines);
			}

			if(both || !debug) {
				AppendDefineConstants (debugDefinesNode, defines);
			}
		}

		void AppendDefineConstants(XmlNode node, string[] defines) {
			string current = node.InnerText;
			current = current.TrimEnd (new char[] { ';' });
			current = current + ";" + string.Join (";", defines);
			node.InnerText = current;
		}

		public void AddAssemblyRef(string name, string path) {
			XmlNode refNode = CreateUnityAssemblyNode (name);
			SetHintPath (refNode, path);
		}

		void SetHintPath (XmlNode referenceNode, string path) {
			XmlNode hintPath = null;
			if (referenceNode != null) {
				hintPath = referenceNode["HintPath"];
				if (hintPath == null)
					hintPath = document.CreateNode (XmlNodeType.Element, "HintPath", "http://schemas.microsoft.com/developer/msbuild/2003");
				hintPath.InnerText = path;
				referenceNode.AppendChild (hintPath);
			}
		}
	}
}
