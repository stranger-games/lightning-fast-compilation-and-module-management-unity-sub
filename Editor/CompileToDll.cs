﻿// Copyright (c) Stranger Games http://www.stranger-games.com/ . All rights reserved.
// Licensed under the GPLv2 license. See LICENSE file in the project root.

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.CodeDom.Compiler;
using System.Diagnostics;
using Microsoft.CSharp;
using System.IO;
using System.Linq;
using System;
using System.Reflection;
using System.Xml.Linq;
using System.Xml;
using UnityEditor.TreeViewExamples;
using Debug = UnityEngine.Debug;
using System.Text.RegularExpressions;

namespace StrangerGames.FastCompile {

	public class CompileToDll {

		static CompileToDll _Instance;
		static public CompileToDll Instance {
			get {
				return _Instance;
			}
		}

		const string UnityEngineModuleRegex = @"UnityEngine.*ll\b";
		const bool UseUnityEngineDLL = false;

		static CompileToDll() {
			TableNodeTree.OtherFunction += Common.OpenSolution;
			CreateInstance();
		}

		static void CreateInstance() {
			Type calcType = Type.GetType("StrangerGames.FastCompile.ProFeatures");
			if(calcType == null)
				_Instance = new CompileToDll();
			else
				_Instance = (CompileToDll)Activator.CreateInstance(calcType);
		}

		public CompileToDll() {
			_Instance = this;
		}

		class QueueParameters {
			public CompilerParameters parameters;
			public string[] source;
			public bool mod;
			public CSProjHelper csProj;
			public string OutputAssembly;
			public OutputDLLType outputDllType;
			public FastCompilationLibrary library;
			public bool isPlugin;
		}

		public class OutputDLLData {
			public string dllPath;
			public OutputDLLType outputDllType;
			public string newGuid;
			public FastCompilationLibrary library;
			public bool isPlugin;
		}

		static Queue<QueueParameters> compileQueue = new Queue<QueueParameters>();
		static List<string> outputFiles = new List<string> ();
		static List<OutputDLLData> outputDlls = new List<OutputDLLData> ();
		static int compiledCount = 0;
		static int queueTotalCount = 0;

		static bool queueMeansDependency = false;

		static List<string> runtimeDlls = new List<string> ();
		static List<string> runtimeEditorDlls = new List<string> ();
		static List<string> editorDlls = new List<string> ();

		static CSharpCodeProvider codeProvider = new CSharpCodeProvider (
						   new Dictionary<string, string> { { "CompilerVersion", "v6.0" } });


		[MenuItem ("Tools/Fast Compilation/Create New")]
		public static void CreateNew() {
			NewFastCompilationLibraryWindow.GetWindow();
		}

		public virtual void ConvertDirectory(FastCompilationLibrary newNode) {
			#if SG_FUTURE
			bool result = EditorUtility.DisplayDialog("Pro Feature", "Please get the pro version to use this feature", "Get Pro", "Cancel");
			if(result) {
				Application.OpenURL();
			}
			#endif
			return;
        }

		protected static void CreateSln(string slnPath, List<CSProjHelper> csProjs) {

            string template = GetSLNTemplateFile();
			var xmlFile = new CSSlnHelper (template, csProjs, slnPath);

			xmlFile.Clear ();

			xmlFile.Save (slnPath);
		}

		public static void UpdateNodeVSFiles(FastCompilationLibrary node) {
			if(node.check[0] || node.check[1] || node.check[2]) {
				string[] sourceFiles = null;
				string[] sourceEditorFiles = null;
				
				if(node.check[0] || node.check[2]) {
					Directory.CreateDirectory(Common.FullPath(Path.GetDirectoryName(node.runtimeProjSubDir)));
					sourceFiles = Directory.GetFiles (Path.GetDirectoryName(node.runtimeProjSubDir), "*.cs", SearchOption.AllDirectories);
					sourceFiles = sourceFiles.Where(x => !x.Contains(Path.Combine(Path.GetDirectoryName(node.runtimeProjSubDir), "obj"))).ToArray();
				}
				else {
					if(File.Exists(node.runtimeProjSubDir))
						File.Delete(node.runtimeProjSubDir);
				}

				if(node.check[1]) {
					Directory.CreateDirectory(Common.FullPath(Path.GetDirectoryName(node.editorProjSubDir)));
					sourceEditorFiles = Directory.GetFiles (Path.GetDirectoryName(node.editorProjSubDir), "*.cs", SearchOption.AllDirectories);
					sourceEditorFiles = sourceEditorFiles.Where(x => !x.Contains(Path.Combine(Path.GetDirectoryName(node.editorProjSubDir), "obj"))).ToArray();
				}
				else {
					if(File.Exists(node.editorProjSubDir))
						File.Delete(node.editorProjSubDir);
				}

				CreateCSProj(node, false, sourceFiles, sourceEditorFiles);
			}
						
			if(node.checkPlugin[0] || node.checkPlugin[1] || node.checkPlugin[2]) {
				string[] sourcePluginFiles = null;
				string[] sourcePluginEditorFiles = null;

				if(node.checkPlugin[0] || node.checkPlugin[2]) {
					Directory.CreateDirectory(Common.FullPath(Path.GetDirectoryName(node.pluginsRuntimeProjSubDir)));
					sourcePluginFiles = Directory.GetFiles (Path.GetDirectoryName(node.pluginsRuntimeProjSubDir), "*.cs", SearchOption.AllDirectories);
				}
				else {
					if(File.Exists(node.pluginsRuntimeProjSubDir))
						File.Delete(node.pluginsRuntimeProjSubDir);
				}

				if(node.checkPlugin[1]) {
					Directory.CreateDirectory(Common.FullPath(Path.GetDirectoryName(node.pluginsEditorProjSubDir)));
					sourcePluginEditorFiles = Directory.GetFiles (Path.GetDirectoryName(node.pluginsEditorProjSubDir), "*.cs", SearchOption.AllDirectories);
				}
				else {
					if(File.Exists(node.pluginsEditorProjSubDir))
						File.Delete(node.pluginsEditorProjSubDir);
				}

				CreateCSProj(node, true, sourcePluginFiles, sourcePluginEditorFiles);
			}

			Common.FixScriptingDefines(node);

			Directory.CreateDirectory(Common.FullPath(Path.GetDirectoryName(node.solutionSubDir)));
			CreateSln(node);
		}

		public static void CreateSln(FastCompilationLibrary newNode) {
			CSProjHelper runtimeProj = null;
			CSProjHelper editorProj = null;
			CSProjHelper pluginsRuntimeProj = null;
			CSProjHelper pluginsEditorProj = null;

			if(File.Exists(newNode.runtimeProjSubDir))
				runtimeProj = new CSProjHelper(newNode.runtimeProjSubDir);
			if(File.Exists(newNode.editorProjSubDir))					
				editorProj = new CSProjHelper(newNode.editorProjSubDir);
			if(File.Exists(newNode.pluginsRuntimeProjSubDir))
				pluginsRuntimeProj = new CSProjHelper(newNode.pluginsRuntimeProjSubDir);
			if(File.Exists(newNode.pluginsEditorProjSubDir))
				pluginsEditorProj = new CSProjHelper(newNode.pluginsEditorProjSubDir);

			List<CSProjHelper> list = new List<CSProjHelper>();

			if(newNode.check[0] && runtimeProj != null)
				list.Add(runtimeProj);
			if(newNode.check[1] && editorProj != null)
				list.Add(editorProj);

			if(newNode.checkPlugin[0] && pluginsRuntimeProj != null)
				list.Add(pluginsRuntimeProj);
			if(newNode.checkPlugin[1] && pluginsEditorProj != null)
				list.Add(pluginsEditorProj);

            CreateSln(newNode.solutionSubDir, list);
		}

		public static List<CSProjHelper> CreateCSProj(FastCompilationLibrary myLibrary, bool plugin, string[] files, string[] editorFiles) {
			List<string> filesList = null;
			if(files != null)
				filesList = files.ToList();

			List<string> editorFilesList = null;
			if(editorFiles != null)
				editorFilesList = editorFiles.ToList();

			return CreateCSProj(myLibrary, plugin, filesList, editorFilesList);
		}

		public static List<CSProjHelper> CreateCSProj(FastCompilationLibrary myLibrary, bool plugin, List<string> files, List<string> editorFiles) {
			bool[] checks;
			if(plugin) {
				checks = myLibrary.checkPlugin;
			}
			else {
				checks = myLibrary.check;
			}

			List<CSProjHelper> csProjs = new List<CSProjHelper>();

			string template = GetCSProjTemplateFile ();

			string csProjPath = "";

			string targetDir = "";

			string assemblyName = "";
			CSProjHelper xmlFile = null;

			assemblyName = myLibrary.name;

			if(checks[0] || checks[2]) {
				xmlFile = new CSProjHelper (template, false);
			
				if(!plugin)
					csProjPath = Common.FullPath(myLibrary.runtimeProjSubDir);
				else
					csProjPath = Common.FullPath(myLibrary.pluginsRuntimeProjSubDir);
			
				if(plugin) {
					assemblyName = assemblyName + " Plugin";
				}

				targetDir = Path.GetDirectoryName(csProjPath);
			
				xmlFile.Clear ();
				xmlFile.SetAssemblyName (assemblyName + " Runtime");

				CSProjAssemblies (xmlFile, myLibrary, false);

				if(files != null) {
					foreach (var file in files) {
						string relativeFile = Common.RelativePath(Common.ConvertPath(csProjPath), Common.ConvertPath(Common.FullPath(file)));
						xmlFile.AddSrcFile (relativeFile);
					}
				}

				#if NET_4_6
				//xmlFile.SetLangVersion("6");
				//xmlFile.SetTargetFrameworkVersion("v4.6");
				//xmlFile.SetUnityVersion(Application.unityVersion);
				#endif

				xmlFile.Save (csProjPath);
				csProjs.Add(xmlFile);
			}

			if(checks[1]) {
				//create EDITOR project
				var xmlFileEditor = new CSProjHelper (template, false);

				if(!plugin)
					csProjPath = Common.FullPath(myLibrary.editorProjSubDir);
				else
					csProjPath = Common.FullPath(myLibrary.pluginsEditorProjSubDir);

				targetDir = Path.GetDirectoryName(csProjPath);

				xmlFileEditor.Clear ();
				xmlFileEditor.SetAssemblyName (assemblyName + " Editor");

				string runtimeRelease = "";
				if(checks[2])
					runtimeRelease = OutputDLL(myLibrary, OutputDLLType.RUNTIME_EDITOR, plugin, xmlFile, true);
				else if(checks[0])
					runtimeRelease = OutputDLL(myLibrary, OutputDLLType.RUNTIME, plugin, xmlFile);

				if(!string.IsNullOrEmpty(runtimeRelease))
					xmlFileEditor.AddAssemblyRef (Path.GetFileNameWithoutExtension(runtimeRelease), runtimeRelease);

				CSProjAssemblies (xmlFileEditor, myLibrary, true);

				if(editorFiles != null) {
					foreach (var file in editorFiles) {
						string relativeFile = Common.RelativePath(Common.ConvertPath(csProjPath), Common.ConvertPath(Common.FullPath(file)));
						xmlFileEditor.AddSrcFile (relativeFile);
					}
				}

				#if NET_4_6
				//xmlFileEditor.SetLangVersion("6");
				//xmlFileEditor.SetTargetFrameworkVersion("4.6");
				//xmlFileEditor.SetUnityVersion(Application.unityVersion);
				#endif

				xmlFileEditor.Save (csProjPath);
				csProjs.Add(xmlFileEditor);
			}

			return csProjs;
		}

		public static string ReleaseOutput(CSProjHelper csProj, bool debugBuild = false) {
			string parent = Path.GetDirectoryName (csProj.csFilename);
			string subfolder = "Release";
			if(debugBuild)
				subfolder = "Debug";
			return Path.Combine (Path.Combine (Path.Combine(parent, "bin"), subfolder), csProj.assemblyName + ".dll");
		}

		public static string GetCSProjTemplateFile() {
			string dir = Path.Combine(Path.Combine(Application.dataPath, "SG-FastCompilation"), "Templates");
			string csTmplate = Path.Combine (dir, "template.csproj");
			return csTmplate;
		}

        public static string GetSLNTemplateFile()
        {
            string dir = Path.Combine(Path.Combine(Application.dataPath, "SG-FastCompilation"), "Templates");
            string csTmplate = Path.Combine(dir, "Template.sln");
            return csTmplate;
        }

		static void CompileCSProj (QueueParameters parameters) {
			CSProjHelper csproj = parameters.csProj;
			var configuration = "Release";
			if(parameters.library.DebugBuild) {
				configuration = "Debug";
			}
			string unityPath = GetUnityPath ();
			DirectoryInfo dir = new DirectoryInfo (Path.GetDirectoryName (unityPath));
			var monoDir = "Mono";
			/*#if NET_4_6
				monoDir = "MonoBleedingEdge";
			#endif*/
			string xbuild = Path.Combine (Path.Combine (Path.Combine (Path.Combine (Path.Combine (dir.Parent.FullName, "Editor"), "Data"), monoDir), "bin"), "xbuild.bat");
			ProcessStartInfo startInfo = new ProcessStartInfo (xbuild) {
				Arguments = "/p:Configuration=" + configuration + " \"" + csproj.csFilename + "\"",
				UseShellExecute = false,
				RedirectStandardOutput = true,
				CreateNoWindow = false,
			};

			Process process = new Process ();

			process.StartInfo = startInfo;
			process.OutputDataReceived += (sender, args) => {
				if(args.Data.Contains(" error "))
					UnityEngine.Debug.LogError (csproj.csFilename + ": " + args.Data);
				else if (args.Data.Contains (" warning "))
					UnityEngine.Debug.LogWarning (csproj.csFilename + ": " + args.Data);
				else
					UnityEngine.Debug.Log (args.Data);
			};

			System.Threading.ManualResetEvent oSignalEvent = new System.Threading.ManualResetEvent(false);
			process.EnableRaisingEvents = true;
			process.Exited += (sender, e) => {
				try {
					string directory = Path.GetDirectoryName (csproj.csFilename);
				
					var dest = OutputDLL(parameters.library, parameters.outputDllType, parameters.isPlugin, csproj);
					var src = ReleaseOutput (csproj, parameters.library.DebugBuild);
					if(!File.Exists(src)) {
						Debug.LogError("Output dll not found");
						oSignalEvent.Set();
						return;
					}
					Debug.Log("Copying output " + src + " to " + dest);
					Directory.CreateDirectory (Path.GetDirectoryName(dest));
					File.Copy (src, dest, true);
					OutputDLLData outputDll = new OutputDLLData();
					outputDll.dllPath = dest;
					outputDll.outputDllType = parameters.outputDllType;
					outputDll.library = parameters.library;
					outputDll.isPlugin = parameters.isPlugin;

					string []newGUIDs = parameters.library.dllGUIDs;
					if(parameters.isPlugin) {
						newGUIDs = parameters.library.pluginDLLGUIDs;
					}

					if(parameters.outputDllType == OutputDLLType.RUNTIME) {
						outputDll.newGuid = newGUIDs[0];
					}
					else if(parameters.outputDllType == OutputDLLType.EDITOR) {
						outputDll.newGuid = newGUIDs[1];
					}
					else if(parameters.outputDllType == OutputDLLType.RUNTIME_EDITOR) {
						outputDll.newGuid = newGUIDs[2];
					}

					outputDlls.Add(outputDll);
					oSignalEvent.Set();
				}
				catch (Exception exception) {
					Debug.LogError(exception.Message);
					oSignalEvent.Set();
				}
			};

			process.Start ();
			process.BeginOutputReadLine ();
			process.WaitForExit ();
			oSignalEvent.WaitOne();
		}

		public static FastCompilationLibrary[] GetCompileLibraries() 
		{
			List<FastCompilationLibrary> resultDir = new List<FastCompilationLibrary>();
            List<FastCompilationLibrary> depedencies;
            NodeTree nodeTree = NodeTree.GetWindow();
            bool canAdd = true;

            for (int j = 0; j < nodeTree.listNodeTable.Count; j++)
			{                
                if(nodeTree.listNodeTable[j].depth >= 0){

                    depedencies = nodeTree.listNodeTable[j].depedency;                    

                    for (int i = 0; i < depedencies.Count; i++)
                    {
                        canAdd = true;
						
                        for (int k = 0; k < resultDir.Count; k++)
                        {
							if(resultDir[k].name == depedencies[i].name){
                                canAdd = false;
                                break;
                            }
                        }

						if(canAdd)
                            resultDir.Insert(0, depedencies[i]);
                    }

                    canAdd = true;

                    for (int k = 0; k < resultDir.Count; k++)
                    {
                        if (resultDir[k].name == nodeTree.listNodeTable[j].name)
                        {
                            canAdd = false;
                            break;
                        }
                    }

					if(canAdd)
                    	resultDir.Add(nodeTree.listNodeTable[j]);					
				}
			}      

			return resultDir.ToArray();
		}

        static FastCompilationLibrary[] GetCompileLibraries(FastCompilationLibrary node)
        {
            List<FastCompilationLibrary> resultDir = new List<FastCompilationLibrary>();
			//todo the commented code below should be fixed to get libraries in dependencies order
            //List<BaseNodeElement> depedencies = node.depedency;
            //bool canAdd = true;

            GetCompileDirectories(node, resultDir, NodeTree.GetWindow());

            /* for (int i = 0; i < depedencies.Count; i++)
            {
                canAdd = true;

                for (int j = 0; j < resultDir.Count; j++)
                {
					if(resultDir[j].name == depedencies[i].name)
					{
                        canAdd = false;
                        break;
                    }                    
                }

                if (canAdd)
                    resultDir.Add(depedencies[i]);
            }

            canAdd = true;

            for (int j = 0; j < resultDir.Count; j++)
            {
                if (resultDir[j].name == node.name)
                {
                    canAdd = false;
                    break;
                }
            }

            if(canAdd)
            	resultDir.Add(node);  */	

            return resultDir.ToArray();
        }

		public static FastCompilationLibrary[] GetCompileLibrariesThatNeedsBuildProcessing() {
			var libraries = CompileToDll.GetCompileLibraries();
			
			var needsProcessing = libraries.Where(x => {
				if(x.check[0] && x.check[2])
					return true;
				return false;
			}
			).ToArray();

			return needsProcessing;
		}

		public static FastCompilationLibrary[] GetCompileLibrariesThatNeedsPluginBuildProcessing() {
			var libraries = CompileToDll.GetCompileLibraries();
			
			var needsProcessing = libraries.Where(x => {
				if(x.checkPlugin[0] && x.checkPlugin[2])
					return true;
				return false;
			}
			).ToArray();

			return needsProcessing;
		}

		static void GetCompileDirectories(FastCompilationLibrary node, List<FastCompilationLibrary> listNode, NodeTree nodeTree)
		{
            List<FastCompilationLibrary> depedencies = node.depedency;
            bool canAdd = true;            

            for (int j = 0; j < listNode.Count; j++)
            {
                if (listNode[j].name == node.name)
                {
                    Debug.LogError("There is a duplicate library in the directory " + listNode[j].name);
                    EditorUtility.ClearProgressBar();
                    canAdd = false;
                    listNode.Clear();
                    return;
                }
            }

            if (canAdd)
                listNode.Insert(0, node);

            for (int i = 0; i < depedencies.Count; i++)
            {
                canAdd = true;

                for (int j = 0; j < listNode.Count; j++)
                {
                    if (listNode[j].name == depedencies[i].name)
                    {
                        canAdd = false;
                        Debug.LogError("There is a duplicate library in the directory "+ listNode[j].name);
                        EditorUtility.ClearProgressBar();
                        listNode.Clear();
						return;
                    }
                }

                if (canAdd)
                {                    
                    for (int j = 0; j < nodeTree.listNodeTable.Count; j++)
                    {
						if(depedencies[i].name == nodeTree.listNodeTable[j].name)
						{
                            canAdd = false;
                            GetCompileDirectories(nodeTree.listNodeTable[j], listNode, nodeTree);
                            break;
                        }						
                    }

					if(canAdd)
                        listNode.Insert(0, depedencies[i]);					                                                    
                }
            }
        }

		[MenuItem ("Tools/Fast Compilation/Compile")]
		public static void StartCompiling () {
			StartCompiling (null);
		}

        public static void StartCompiling(FastCompilationLibrary node)
        {
            compileQueue.Clear();

            bool forceCompile = false;
            if (compileQueue.Count == 0)
            {
                runtimeDlls.Clear();
                runtimeEditorDlls.Clear();
                editorDlls.Clear();
                outputFiles.Clear();

                EditorUtility.DisplayProgressBar("Lightning Fast Compilation", "Preparing Folder", 0f);
                
				FastCompilationLibrary[] libraries;
				if(node != null)
					libraries = GetCompileLibraries(node);
				else
					libraries = GetCompileLibraries();
                
                foreach (FastCompilationLibrary library in libraries)
                {
					CompileToDll.UpdateNodeVSFiles(library);

					List<string> files = null, editorFiles = null;

					if(library.pluginManaged) {
						if(!string.IsNullOrEmpty(library.VSFilesPluginSrcBaseDirectory)) {
							Directory.CreateDirectory(Path.Combine(library.VSFilesPluginSrcBaseDirectory, "Runtime"));
							Directory.CreateDirectory(Path.Combine(library.VSFilesPluginSrcBaseDirectory, "Editor"));
						}                    

						GetFiles(library.VSFilesPluginSrcBaseDirectory, out files, out editorFiles);
					}
                    CompileFiles(library, files, editorFiles, forceCompile, true);

					if(library.pluginManaged) {
						if(!string.IsNullOrEmpty(library.VSFilesBaseSrcDirectory)) {
							Directory.CreateDirectory(Path.Combine(library.VSFilesBaseSrcDirectory, "Runtime"));
							Directory.CreateDirectory(Path.Combine(library.VSFilesBaseSrcDirectory, "Editor"));
						}                    

						GetFiles(library.VSFilesBaseSrcDirectory, out files, out editorFiles);
					}
                    CompileFiles(library, files, editorFiles, forceCompile, false);
                }

                queueTotalCount = compileQueue.Count;
                compiledCount = 0;

				outputDlls.Clear();
                CleanReferencedAssembliesInQueue();
                while (compileQueue.Count > 0)
                {
                    string fileName = Path.GetFileNameWithoutExtension(compileQueue.Peek().OutputAssembly);
                    EditorUtility.DisplayProgressBar("Lightning Fast Compilation", "Compiling " + fileName, (float)compiledCount / queueTotalCount);
                    ExecuteQueue();                    
                }

                foreach (OutputDLLData output in outputDlls)
                {
                    FixImportForDLL(output);
                }

                EditorUtility.ClearProgressBar();
            }
        }

		static FastCompilationLibrary[] SortDirectories(FastCompilationLibrary[] directories)
		{            
            FastCompilationLibrary tempNode = null;
			
			for(int i = 1; i < directories.Length; i++)
			{
				if(directories[i].depth < directories[i-1].depth)
				{
					tempNode = directories[i];
                    directories[i] = directories[i - 1];
                    directories[i - 1] = tempNode;

					i = 1;
				}
			}			

            return directories;
        }

		static void CleanReferencedAssembliesInQueue() {
			outputFiles.Clear ();
			foreach (QueueParameters parameters in compileQueue) {
				outputFiles.Add (parameters.OutputAssembly);
			}

			foreach (QueueParameters parameters in compileQueue) {
				if (parameters.parameters != null) {
					foreach (string dllPath in outputFiles) {
						parameters.parameters.ReferencedAssemblies.Remove (dllPath);
					}

					List<string> removeThis = new List<string> ();
					for (int i = 0; i < parameters.parameters.ReferencedAssemblies.Count; i++) {
						string referencedAssembly = parameters.parameters.ReferencedAssemblies[i];
						if (referencedAssembly.Contains ("Assembly-CSharp")) {
							removeThis.Add (referencedAssembly);
						}
					}

					foreach (string remove in removeThis) {
						parameters.parameters.ReferencedAssemblies.Remove (remove);
					}
				}
			}
		}

		public enum OutputDLLType {
			RUNTIME,
			RUNTIME_EDITOR,
			EDITOR
		}

		public static string OutputDLL(FastCompilationLibrary library, OutputDLLType dllType, bool isPlugin, CSProjHelper csProj, bool runtimeCSProj = false) {
			string unityOutputDir = "";
			if(!isPlugin)
				unityOutputDir = Common.FullPath(library.baseDirectory);
			else
				unityOutputDir = Common.FullPath(library.pluginOutputBaseDirectory);

			var outputDLLDir = Path.Combine (unityOutputDir, "OutputDLL");
			if(dllType == OutputDLLType.EDITOR) {
				unityOutputDir = Path.Combine (outputDLLDir, "Editor");
			}
			else {
				unityOutputDir = Path.Combine (outputDLLDir, "Runtime");
			}

			var name = library.name;
			var suffix = "";
			if(csProj != null) {
				name = csProj.assemblyName;
				if(dllType == OutputDLLType.RUNTIME_EDITOR && runtimeCSProj) {
					suffix = " Editor";
				}
			}
			else {
				switch (dllType) {
					case OutputDLLType.RUNTIME:
						suffix = " Runtime";
						break;
					case OutputDLLType.RUNTIME_EDITOR:
						suffix = " Runtime Editor";
						break;
					case OutputDLLType.EDITOR:
						suffix = " Editor";
						break;
				}
			}
			var dest = Path.Combine (unityOutputDir, name + suffix + ".dll");
			return dest;
		}

		static bool CompileFiles(FastCompilationLibrary fastCompilationLibrary, List<string> engineFiles, List<string> editorFiles, bool forceBuild, bool isPlugin) {
			string directory;
			bool[] checks;
			string editorProjSubDir;
			string runtimeProjSubDir;
			if(isPlugin) {
				directory = fastCompilationLibrary.VSFilesBaseSrcDirectory;
				checks = fastCompilationLibrary.checkPlugin;
				editorProjSubDir = fastCompilationLibrary.pluginsEditorProjSubDir;
				runtimeProjSubDir = fastCompilationLibrary.pluginsRuntimeProjSubDir;
			}
			else {
				directory = fastCompilationLibrary.VSFilesPluginSrcBaseDirectory;
				checks = fastCompilationLibrary.check;
				editorProjSubDir = fastCompilationLibrary.editorProjSubDir;
				runtimeProjSubDir = fastCompilationLibrary.runtimeProjSubDir;
			}

			if(!File.Exists(editorProjSubDir))
				editorProjSubDir = "";

			if(!File.Exists(runtimeProjSubDir))
				runtimeProjSubDir = "";

			//todo queue the tasks
			string baseEngineDir = directory + System.IO.Path.DirectorySeparatorChar + "Runtime" + System.IO.Path.DirectorySeparatorChar;
			string baseEditorDir = directory + System.IO.Path.DirectorySeparatorChar + "Editor" + System.IO.Path.DirectorySeparatorChar;            			

            bool mod = forceBuild;
			bool singleMod = false;

			string EngineOutput = OutputDLL (fastCompilationLibrary, OutputDLLType.RUNTIME, isPlugin, null);

			bool csProjExist = false;
			bool csProjEditorExist = false;

			string csProj = "", csProjEditor = "";

			if(!string.IsNullOrEmpty(editorProjSubDir)) {
				csProjEditorExist = true;
				csProjEditor = editorProjSubDir;
			}

			if(!string.IsNullOrEmpty(runtimeProjSubDir)) {
				csProjExist = true;
				csProj = runtimeProjSubDir;
			}

            if (checks[0])
            {
                //Cs proj
                if (!csProjExist)
                {
                    singleMod = forceBuild || IsModified(engineFiles, EngineOutput);
                    QueueCompileFolder(baseEngineDir, isPlugin, codeProvider, EngineOutput, singleMod);
                    mod = mod || singleMod;
                }
                else
                {
                    QueueCompileFolder(fastCompilationLibrary, isPlugin, csProj, OutputDLLType.RUNTIME);
                }
            }
            
			string UnityEditorOutput = OutputDLL(fastCompilationLibrary, OutputDLLType.RUNTIME_EDITOR, isPlugin, null);

			bool runtimeEditorMod = false;
			if (IsModified(engineFiles, UnityEditorOutput))
			{
				runtimeEditorMod = true;
			}

            if (checks[2])
            {
                //RuntimeEditor cs proj
                if (!csProjExist)
                {
                    singleMod = forceBuild || runtimeEditorMod;
                    QueueCompileFolder(baseEngineDir, isPlugin, codeProvider, UnityEditorOutput, singleMod, "/d:UNITY_EDITOR");
                    mod = mod || singleMod;
                }
                else
                {
                    QueueCompileFolder(fastCompilationLibrary, isPlugin, csProj, OutputDLLType.RUNTIME_EDITOR, "UNITY_EDITOR", " Editor");
                }
            }

            if (checks[1])
            {
                string EditorOutput = OutputDLL(fastCompilationLibrary, OutputDLLType.EDITOR, isPlugin, null);

                //Editor cs proj
                if (!csProjEditorExist)
                {
                    singleMod = forceBuild || runtimeEditorMod || IsModified(editorFiles, EditorOutput);
                    QueueCompileFolder(baseEditorDir, isPlugin, codeProvider, EditorOutput, singleMod, "/d:UNITY_EDITOR");
                    mod = mod || singleMod;
                }
                else
                {
                    QueueCompileFolder(fastCompilationLibrary, isPlugin, csProjEditor, OutputDLLType.EDITOR);
                }
            }

            return mod;
		}

		static bool IsModified(List<string> files, string dllFile) {
			if(!File.Exists(dllFile))
				return true;

			FileInfo dllInfo = new FileInfo (dllFile);
			DateTime lessTime = dllInfo.CreationTime;
			if(dllInfo.LastWriteTime < lessTime) {
				lessTime = dllInfo.LastWriteTime;
			}
			bool modified = false;

			foreach(string file in files) {
				FileInfo fileInfo = new FileInfo (file);

				DateTime moreTime = fileInfo.CreationTime;
				if (fileInfo.LastWriteTime > moreTime) {
					moreTime = fileInfo.LastWriteTime;
				}

				if (moreTime > lessTime) {
					modified = true;
					break;
				}
			}

			return modified;
		}

		static void FixImportForDLL(OutputDLLData output) {
			bool[] check;
			if(!output.isPlugin) {
				check = output.library.check;
			}
			else {
				check = output.library.checkPlugin;
			}

			string relativePath = FastCompilationLibrary.ShortenPath(Common.ConvertPath(output.dllPath));
			if (File.Exists (output.dllPath)) {

				PluginImporter importer = AssetImporter.GetAtPath (relativePath) as PluginImporter;
				if(importer == null) {
					AssetDatabase.ImportAsset (relativePath);
					importer = AssetImporter.GetAtPath (relativePath) as PluginImporter;
				}

				bool changed = false;

				if(check[2]) {
					if (output.outputDllType == OutputDLLType.RUNTIME) {
						if (!importer.GetExcludeEditorFromAnyPlatform () || !importer.GetCompatibleWithAnyPlatform ()) {
							importer.SetCompatibleWithAnyPlatform (true);
							importer.SetExcludeEditorFromAnyPlatform (true);
							changed = true;
						}
					} else if (output.outputDllType == OutputDLLType.RUNTIME_EDITOR) {
						if (importer.GetCompatibleWithAnyPlatform () || !importer.GetCompatibleWithEditor () || importer.GetCompatibleWithPlatform(EditorUserBuildSettings.activeBuildTarget)) {
							importer.ClearSettings ();
							importer.SetCompatibleWithAnyPlatform (false);
							importer.SetCompatibleWithEditor (true);
							changed = true;
						}
					}
				}

				if(changed) {
					importer.SaveAndReimport ();
				}

				if(!(check[2] && output.outputDllType == OutputDLLType.RUNTIME)) {
					ReplaceGUIDInMetaFile(output.dllPath + ".meta", output.newGuid);
				}

				AssetDatabase.ImportAsset(relativePath, ImportAssetOptions.ForceUpdate);
			}
		}

		private static bool IsGuid(string text) {
			return text.All(x => char.IsLetterOrDigit(x));
        }

		public static IEnumerable<string> GetGuidsFromMetaFile(string metaFile, out string contents) {
			contents = File.ReadAllText(metaFile);
			return GetGuids(contents);
		}

		public static void ReplaceGUIDInMetaFile(string metaFile, string newGuid) {
			string contents;
			var guids = GetGuidsFromMetaFile(metaFile, out contents);
			contents = contents.Replace("guid: " + guids.First(), "guid: " + newGuid);
			File.WriteAllText(metaFile, contents);
		}

		private static IEnumerable<string> GetGuids(string text) {
            const string guidLabel = "guid: ";
            const int guidStrLength = 32;
            List<string> guidList = new List<string>();

            int index = 0;
            while (index + guidLabel.Length + guidStrLength < text.Length) {
                index = text.IndexOf(guidLabel, index, StringComparison.Ordinal);
                if (index == -1)
                    break;

                index += guidLabel.Length;
                string guid = text.Substring(index, guidStrLength);
                index += guidStrLength;

                if (IsGuid(guid)) {
                    guidList.Add(guid);
                }
            }

            return guidList;
        }

		static string GetUnityDLL(string dllName) {
			string unityPath = GetUnityPath ();
			DirectoryInfo dir = new DirectoryInfo (Path.GetDirectoryName (unityPath));
			string managedPath = Path.Combine (Path.Combine (dir.FullName, "Data"), "Managed");

			string fullPath = Path.Combine (managedPath, dllName);

			return fullPath;
		}

		static string GetUnityPath() {
			return Process.GetCurrentProcess ().MainModule.FileName;
		}

		static void ExecuteQueue() {
			QueueParameters parameter = compileQueue.Dequeue ();

			if (parameter.csProj == null) {
				DateTime lastDateTimeRuntime = LastDateTime (runtimeDlls);
				DateTime lastDateTimeRuntimeEditor = LastDateTime (runtimeEditorDlls);
				DateTime lastDateTimeEditor = LastDateTime (editorDlls);

				DateTime lastTime = new DateTime (1995, 10, 1);

				if (parameter.parameters.OutputAssembly.Contains ("Runtime.dll")) {
					parameter.parameters.ReferencedAssemblies.AddRange (runtimeDlls.ToArray ());
					lastTime = lastDateTimeRuntime;
				} else if (parameter.parameters.OutputAssembly.Contains ("RuntimeEditor.dll")) {
					parameter.parameters.ReferencedAssemblies.AddRange (runtimeEditorDlls.ToArray ());
					lastTime = lastDateTimeRuntimeEditor;
				} else if (parameter.parameters.OutputAssembly.Contains ("UnityEditor.dll")) {
					parameter.parameters.ReferencedAssemblies.AddRange (runtimeEditorDlls.ToArray ());
					parameter.parameters.ReferencedAssemblies.AddRange (editorDlls.ToArray ());
					lastTime = lastDateTimeRuntimeEditor;
					if (lastDateTimeEditor > lastTime) {
						lastTime = lastDateTimeEditor;
					}
				}

				if (!queueMeansDependency) {
					lastTime = new DateTime (1995, 10, 1);
				}

				FileInfo dllInfo = new FileInfo (parameter.parameters.OutputAssembly);
				DateTime lessTime = dllInfo.CreationTime;
				if (dllInfo.LastWriteTime < lessTime) {
					lessTime = dllInfo.LastWriteTime;
				}

				bool dllMod = lessTime < lastTime;

				Directory.CreateDirectory (Path.GetDirectoryName (parameter.parameters.OutputAssembly));

				CompilerResults results = null;

				UnityEngine.Debug.Log ("Task: " + (compiledCount + 1) + "/" + queueTotalCount);
				if (parameter.mod || dllMod) {
					UnityEngine.Debug.Log ("Compiling: " + parameter.parameters.OutputAssembly);
					results = codeProvider.CompileAssemblyFromSource (parameter.parameters, parameter.source);
					UnityEngine.Debug.Log ("Finished Compiling " + parameter.parameters.OutputAssembly + " with " + results.Errors.Count + " errors");
				} else {
					UnityEngine.Debug.Log ("No Src Modification Detected, Skipping Compiling " + parameter.parameters.OutputAssembly);
				}
				compiledCount++;

				if (File.Exists (parameter.parameters.OutputAssembly)) {
					if (parameter.parameters.OutputAssembly.Contains ("Runtime.dll")) {
						runtimeDlls.Add (parameter.parameters.OutputAssembly);
					} else if (parameter.parameters.OutputAssembly.Contains ("RuntimeEditor.dll")) {
						runtimeEditorDlls.Add (parameter.parameters.OutputAssembly);
					} else if (parameter.parameters.OutputAssembly.Contains ("UnityEditor.dll")) {
						editorDlls.Add (parameter.parameters.OutputAssembly);
					}
				}
				ShowErrors (results);
			}
			else {
				CompileCSProj (parameter);
			}
		}

		static DateTime LastDateTime(List<string> files) {
			DateTime lastTime = new DateTime ();

			foreach (string file in files) {
				FileInfo fileInfo = new FileInfo (file);

				DateTime moreTime = fileInfo.CreationTime;
				if (fileInfo.LastWriteTime > moreTime) {
					moreTime = fileInfo.LastWriteTime;
				}

				if (moreTime > lastTime) {
					lastTime = moreTime;
				}
			}

			return lastTime;
		}

		static void QueueCompileFolder(string dir, bool isPlugin, CSharpCodeProvider codeProvider, string output, bool mod, string additionalOptions = "") {
			CompilerParameters parameters = new CompilerParameters ();
			parameters.GenerateExecutable = false;

			string defaultOptions = "";
			foreach (string option in UnityEditor.EditorUserBuildSettings.activeScriptCompilationDefines) {
				defaultOptions += " /d:" + option;
			}
			parameters.CompilerOptions = additionalOptions + defaultOptions;

			parameters.OutputAssembly = output;

			System.Reflection.Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies ();
			parameters.ReferencedAssemblies.Clear ();
			foreach (var loadedAssembly in assemblies) {
				string path = loadedAssembly.Location;
				FixPath (ref path);
				
				if(UseUnityEngineDLL && Regex.Match(path, UnityEngineModuleRegex).Success) {
					path = Regex.Replace(path, UnityEngineModuleRegex, "UnityEngine.dll");
				}

				if(!parameters.ReferencedAssemblies.Contains(path))
					parameters.ReferencedAssemblies.Add (path);
			}

			parameters.CompilerOptions += " /recurse:\"" + dir + "*.cs\"";            

            QueueParameters queueParameter = new QueueParameters ();
			queueParameter.parameters = parameters;
			queueParameter.source = new string[1] { "" };
			queueParameter.mod = mod;
			queueParameter.isPlugin = isPlugin;
			compileQueue.Enqueue (queueParameter);
		}

		static void QueueCompileFolder(FastCompilationLibrary fastCompilationLibrary, bool isPlugin, string csProj, OutputDLLType outputDllType, string additionalDefine = "", string suffix = "") {
			var xmlFile = new CSProjHelper (csProj, false);

			xmlFile.AddDefineConstants (additionalDefine);

			if(outputDllType == OutputDLLType.RUNTIME_EDITOR) {
				xmlFile.ClearRefernceAssemblies();
				CSProjAssemblies(xmlFile, fastCompilationLibrary, false, true);
			}

			xmlFile.SetAssemblyName (xmlFile.assemblyName + suffix);

			string dir = Path.GetDirectoryName (csProj);
			string name = Path.GetFileNameWithoutExtension (csProj);

			xmlFile.Save (Path.Combine (dir, name + suffix + "_temp.csproj"));

			QueueParameters queueParameter = new QueueParameters ();
			queueParameter.csProj = xmlFile;
			queueParameter.outputDllType = outputDllType;
			queueParameter.library = fastCompilationLibrary;
			queueParameter.isPlugin = isPlugin;
			compileQueue.Enqueue (queueParameter);            
        }

		static List<string> AllFastLibraryNames() {
			List<string> assemblies = new List<string>();
			NodeTree nodeTree = NodeTree.GetWindow();
			for (int j = 0; j < nodeTree.listNodeTable.Count; j++) {
				var library = nodeTree.listNodeTable[j];

				assemblies.Add(library.name);
			}

			return assemblies;
		}

		static List<string> AllFastLibraryOutputs(List<string> libraryNames) {
			return libraryNames.Select(x =>
			{
				var y = x + @".*ll\b";
				return y;
			}).ToList();
		}

		static bool AssemblyMatchAnyLibrary(string assembly, List<string> libraryOutputRegex) {
			bool matchAny = false;
			assembly = Path.GetFileName(assembly);
			foreach(var regex in libraryOutputRegex) {
				if(Regex.IsMatch(assembly, regex)) {
					matchAny = true;
					break;
				}
			}

			return matchAny;
		}

		static void CSProjAssemblies(CSProjHelper csproj, FastCompilationLibrary library, bool isEditor, bool runtimeEditor = false) {
			Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies ();
			bool addedUnityEngine = false;
			var excludeNames = AllFastLibraryNames();
			var excludeRegexs = AllFastLibraryOutputs(excludeNames);
			foreach (var loadedAssembly in assemblies) {
			#if NET_4_6
				if(loadedAssembly.IsDynamic)
					continue;
			#endif
				if (!loadedAssembly.Location.Contains ("Assembly-CSharp")) {
					string path = loadedAssembly.Location;
					FixPath (ref path);
					if(UseUnityEngineDLL && Regex.Match(path, UnityEngineModuleRegex).Success) {
						if(addedUnityEngine)
							continue;
						path = Regex.Replace(path, UnityEngineModuleRegex, "UnityEngine.dll");
						addedUnityEngine = true;
					}

					if(AssemblyMatchAnyLibrary(path, excludeRegexs)) {
						continue;
					}

					csproj.AddAssemblyRef (Path.GetFileNameWithoutExtension (path), path);
				}
			}

			foreach(var dependency in library.depedency) {
				string assembly = "";
				var type = OutputDLLType.RUNTIME;
				if(runtimeEditor) {
					type = OutputDLLType.RUNTIME_EDITOR;
				}

				if(dependency.checkPlugin[0]) {
					assembly = OutputDLL(dependency, type, true, null);
				}

				if(!string.IsNullOrEmpty(assembly))
					csproj.AddAssemblyRef (Path.GetFileNameWithoutExtension(assembly), assembly);
				
				assembly = "";
				if(dependency.check[0]) {
					assembly = OutputDLL(dependency, type, false, null);
				}

				if(!string.IsNullOrEmpty(assembly))
					csproj.AddAssemblyRef (Path.GetFileNameWithoutExtension(assembly), assembly);
				
				if(isEditor) {
					assembly = "";
					if(dependency.checkPlugin[1]) {
						assembly = OutputDLL(dependency, OutputDLLType.EDITOR, true, null);
					}

					if(!string.IsNullOrEmpty(assembly))
						csproj.AddAssemblyRef (Path.GetFileNameWithoutExtension(assembly), assembly);
					
					assembly = "";
					if(dependency.check[1]) {
						assembly = OutputDLL(dependency, OutputDLLType.EDITOR, false, null);
					}

					if(!string.IsNullOrEmpty(assembly))
						csproj.AddAssemblyRef (Path.GetFileNameWithoutExtension(assembly), assembly);
				}
			}
		}

		static CompilerResults CompileFromFiles(CompilerParameters parameters, List<string> files, CSharpCodeProvider codeProvider) {
			CompilerResults results = codeProvider.CompileAssemblyFromSource (parameters, files.ToArray());
			return results;
		}

		static void ShowErrors(CompilerResults results) {
			if (results == null)
				return;
			foreach (CompilerError CompErr in results.Errors) {
				string error = CompErr.FileName +
							"\nLine number " + CompErr.Line +
							", Error Number: " + CompErr.ErrorNumber +
							", '" + CompErr.ErrorText;
				if(CompErr.IsWarning)
					UnityEngine.Debug.LogWarning (error);
				else
					UnityEngine.Debug.LogError (error);
			}
		}

		protected static void GetFiles(string dir, out List<string> engineFiles, out List<string> editorFiles) {
			if(string.IsNullOrEmpty(dir)) {
                engineFiles = null;
                editorFiles = null;
                return;
            }

			engineFiles = new List<string> ();
			editorFiles = new List<string> ();

			string[] allfiles = System.IO.Directory.GetFiles (dir, "*.cs", System.IO.SearchOption.AllDirectories);
			foreach(string file in allfiles) {
				string[] pathEntries = file.Split (new char[] { '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);
				bool editor = false;
				bool asset = false;

				foreach(string pathEntry in pathEntries) {
					if(pathEntry == "Assets") {
						asset = true;
					}
					if(asset && pathEntry == "Editor") {
						editor = true;
					}
				}

				if (editor) {
					editorFiles.Add (file);
				}
				else {
					engineFiles.Add (file);
				}
			}
		}

		protected static void FixPath(ref string path) {
			if (Application.platform == RuntimePlatform.WindowsEditor)
				path = path.Replace ("/", "\\");
		}

		public static void BuildProcessing(FastCompilationLibrary library, bool isPlugin, OutputDLLType outputDLLType, bool generateNewGUID = false) {
			var csproj = new CSProjHelper (library.runtimeProjSubDir, false);
			var dest = OutputDLL(library, outputDLLType, isPlugin, csproj, true);
			OutputDLLData outputDll = new OutputDLLData();
			outputDll.dllPath = dest;
			outputDll.outputDllType = outputDLLType;
			outputDll.library = library;
			outputDll.isPlugin = isPlugin;

			string []newGUIDs = library.dllGUIDs;
			if(isPlugin) {
				newGUIDs = library.pluginDLLGUIDs;
			}

			if(generateNewGUID) {
				outputDll.newGuid = Guid.NewGuid().ToString();
			}
			else {
				outputDll.newGuid = newGUIDs[0];
			}

			string relativePath = FastCompilationLibrary.ShortenPath(Common.ConvertPath(outputDll.dllPath));

			ReplaceGUIDInMetaFile(outputDll.dllPath + ".meta", outputDll.newGuid);

			AssetDatabase.ImportAsset(relativePath, ImportAssetOptions.ForceUpdate | ImportAssetOptions.ForceSynchronousImport);
		}
	}
}
