﻿// Copyright (c) Stranger Games http://www.stranger-games.com/ . All rights reserved.
// Licensed under the GPLv2 license. See LICENSE file in the project root.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using StrangerGames.FastCompile;
using System.IO;
using System;

namespace StrangerGames.FastCompile {
	public class NewFastCompilationLibraryWindow : EditorWindow {

		FastCompilationLibrary node;
		string libraryName;
		bool showAdvanced;

		public static NewFastCompilationLibraryWindow GetWindow()
		{
			var window = GetWindow<NewFastCompilationLibraryWindow>();

			window.titleContent = new GUIContent("New Fast Compilation Library");
			window.Focus();
			window.Repaint();

			return window;
		}

		/// <summary>
		/// OnGUI is called for rendering and handling GUI events.
		/// This function can be called multiple times per frame (one call per event).
		/// </summary>
		void OnGUI()
		{
			bool ready = true;
			Color baseColor = GUI.backgroundColor;

			GUILayout.BeginHorizontal();

			GUILayout.Label("Library Name", "box", GUILayout.Width(300));
        
			libraryName = GUILayout.TextField(libraryName, GUILayout.Width(250));

			GUILayout.EndHorizontal();

			Common.DrawGUI(ref node, ref showAdvanced);

			if (!ready)
			{
				EditorGUI.BeginDisabledGroup(true);
				GUILayout.Button("Create");
				EditorGUI.EndDisabledGroup();
			}
			else
			{
				if(GUILayout.Button("Create")){
					try {
						//new libraries from this wizard are always under Assets + LibraryName ('.' in name is subfolder) folder with Assets + Plugins + LibraryName ('.' in name is subfolder) folder for plugins

						var dirs = libraryName.Split('.');
						var fullBaseDirectory = Application.dataPath;
						foreach(var dir in dirs) {
							fullBaseDirectory += Path.DirectorySeparatorChar + dir;
						}

						node.pluginManaged = true;
						var libraryFinalName = dirs[dirs.Length - 1];
						node.name = libraryFinalName;
					
						node.baseDirectory = fullBaseDirectory;
						node.pluginOutputBaseDirectory = Application.dataPath + Path.DirectorySeparatorChar + "Plugins" + Path.DirectorySeparatorChar + libraryFinalName;

						Directory.CreateDirectory(fullBaseDirectory);

						if(File.Exists(fullBaseDirectory + Path.DirectorySeparatorChar + libraryFinalName + ".asset")) {
							throw new Exception("Library already exist");
						}

						AssetDatabase.CreateAsset(node, node.baseDirectory + Path.DirectorySeparatorChar + libraryFinalName + ".asset");

						try {
							CompileToDll.UpdateNodeVSFiles(node);

							AssetDatabase.SaveAssets();
							AssetDatabase.Refresh();

							EditorUtility.DisplayDialog(Common.InstructionsTitle, Common.Instructions, "OK :)");

							//UICommon.OpenSolution(node);
							Close();
							return;
						}
						catch(Exception e) {
							//Close();
							throw e;
						}
					}
					catch(Exception e) {
						Debug.LogError("Library creation failed with error: " + e.Message);
						Debug.LogError(e.StackTrace);
						EditorUtility.DisplayDialog("Library creation failed", e.Message, "OK :(");
					}
				}
			}
		}

		private void OnEnable() {
			Debug.Log("New Library Window Enabled");
			
			libraryName = "New Library";
			node = new FastCompilationLibrary(libraryName, 0);
			node.checkPlugin[0] = node.checkPlugin[1] = node.checkPlugin[2] = false;
			node.check[1] = node.check[2] = false;
		}
	}
}
