This project is licensed under the GPLv2 license (see LICENSE).

# Lightning Fast Compilation and Module Management Unity

**Lightning Fast Compilation and Module Management for Unity**

Lightning Fast Compilation and Module Management is a great asset that helps developers working on large projects who need flexibility and to divide the scripts into self contained libraries that compile separately and potentially stored as an independent subrepository.

Lightning Fast Compilation and Module Management makes you create self contained scripting library with those features:

* Flexibility of having source code with the speed of having a compiled DLL
* You can have editor code along with run-time code and also Plug-in code in the same library
* Use platform and custom defined directives as you wish in your library code
* Your library can be a self contained git subrepository that integrates easily in multiple game's codes
* If you were using a DLL before, with the new Fast Compilation DLL you can have the same project and you don't have to re-assign MonoBehaviours
* With 'Lightning Fast Compilation and Module Management for Unity' you will have your DLL compiled and linked with the running version of Unity's assemblies avoiding any compatibility issues.

---

## What is it all about!

Imagine your next big game code. You probably are using a lot of assets code, a lot of your own modular code, and a lot of third party libraries. Things can get messy especially if you need to customize a third party asset or library code for a particular game and manage them as sub-repositories.


Another problem is everything have to be recompiled if you even added a single line of code to test something out. That can be a nuisance in your Edit Code -> Play Test cycle.


You might think that compiling some code to DLL solve some of those problems, and you are right, but what if you have updated some define symbols to the source code of the DLL? You will need to leave unity, open visual studio, change the define symbols manually to match unity and recompile, which is quite nuisance if you ask me. Our asset is to automate and organize those kind of processes with a versioning system friendly approach.


Please check the [documentation](http://www.stranger-games.com/lightning-fast-compilation-module-management-unity/) and [tutorials](http://www.stranger-games.com/lightning-fast-compilation-module-management-unity-tutorials/)

---

## Getting Started

If you are using git with your unity project please add the subrepository here as a submodule to your project.

The subrepository [URL](https://bitbucket.org/stranger-games/lightning-fast-compilation-and-module-management-unity-sub/) should be added to the subrepository path Assets/SG-FastCompilation

If you are not using git please clone the subrepository with [URL](https://bitbucket.org/stranger-games/lightning-fast-compilation-and-module-management-unity-sub/src/master/) to Assets/SG-FastCompilation

Then check the [documentation](http://www.stranger-games.com/lightning-fast-compilation-module-management-unity/) and [tutorials](http://www.stranger-games.com/lightning-fast-compilation-module-management-unity-tutorials/)

Alternatively you can check the asset on the [Unity's asset store page](https://assetstore.unity.com/packages/slug/120668)
